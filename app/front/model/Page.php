<?php

/**
 * 单页模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-08-03
 */

namespace app\front\model;

use fw\base\Instance;

class Page extends \app\common\model\Page
{
    use Instance;
}
