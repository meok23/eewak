<?php

/**
 * 文档模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\model;

use fw\base\Instance;

class Doc extends \app\common\model\Doc
{
    use Instance;
}
