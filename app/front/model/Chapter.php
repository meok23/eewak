<?php

/**
 * 文档章节模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\model;

use fw\base\Instance;
use fw\tool\Url;

class Chapter extends \app\common\model\Chapter
{
    use Instance;

    /**
     * 章节树
     *
     * @param array $rows
     * @param int   $parent_id
     *
     * @return string
     */
    public function ChapterTree($rows, $parent_id = 0)
    {
        $html = '';
        foreach ($rows as $k => $v) {
            if ($v['parent_id'] == $parent_id) {
                $url = Url::to('Chapter/show', ['id' => $v['chapter_id']]);

                if ($v['types'] === 'dir') {
                    $title = '<a class="tree-dir" href="javascript:;">' . $v['title'] . '</a>';
                } else {
                    $title = '<a class="tree-content" href="' . $url . '">' . $v['title'] . '</a>';
                }

                $html .= '<li data-id="' . $v['chapter_id'] . '"><p>' . $title . '</p>';
                $html .= $this->ChapterTree($rows, $v['chapter_id']);
                $html = $html . '</li>';
            }
        }

        return $html ? '<ul>' . $html . '</ul>' : $html;
    }
}
