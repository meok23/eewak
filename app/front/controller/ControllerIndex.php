<?php

/**
 * 首页控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\controller;

use fw\base\Conf;
use fw\base\Rout;
use fw\base\View;
use fw\db\DB;

class ControllerIndex extends Base
{
    public function index()
    {
        $index_res = explode('/', Conf::$all['index_resource']);
        Rout::gi()->exec($index_res[0], $index_res[1]);
    }

    public function tags()
    {
        $sql = "SELECT DISTINCT tag FROM `tag_rel` WHERE channel='art'";
        $ps = DB::$dbh->query($sql);
        $ret = $ps->fetchAll(\PDO::FETCH_COLUMN);
        $data = array_chunk($ret, 15);

        // 输出到视图
        View::render('index_tags', ['data' => $data]);
    }
}
