<?php

/**
 * 文章控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\controller;

use app\front\model\Art;
use fw\base\Conf;
use fw\base\View;
use fw\db\DB;
use fw\tool\Request;

class ControllerArt extends Base
{
    public function index()
    {
        // 参数获取
        $length = Conf::$all['front_art_length'];
        $current_page = (int)Request::get('page', null, 1);

        // 条数总计
        $list_count = Art::gi()->getList([
            'is_hide'  => 0,
            'is_count' => true,
        ]);
        $page_total = ceil($list_count / $length);

        // 分页
        $option = [
            'current_page' => $current_page,
            'page_total'   => $page_total,
            'list_count'   => $list_count,
        ];
        $page = new \app\common\logic\Page($option);
        $page_show = $page->show();

        // 查询列表
        $data = Art::gi()->getList([
            'is_hide'  => 0,
            'length' => $length,
            'page'   => $current_page,
        ]);

        // 输出到视图
        View::render('art_index', ['page_show' => $page_show, 'data' => $data]);
    }

    public function search()
    {
        // 参数获取
        $length = Conf::$all['front_art_length'];
        $current_page = (int)Request::get('page', null, 1);
        $tag = Request::get('tag', 'trim');

        // 条数总计
        $sql = "SELECT COUNT(*) AS cc FROM `art` a LEFT JOIN `tag_rel` t ON a.art_id=t.obj_id AND t.channel='art' WHERE a.is_hide=0 AND t.tag='{$tag}'";
        $rt = DB::queryRow($sql);
        $list_count = $rt['cc'];

        if ($list_count < 1) {

        }
        $page_total = ceil($list_count / $length);

        // 分页
        $option = [
            'current_page' => $current_page,
            'page_total'   => $page_total,
            'list_count'   => $list_count,
        ];
        $page = new \app\common\logic\Page($option);
        $page_show = $page->show();

        // 查询列表
        $limit = ($current_page - 1) * $length . ',' . $length;
        $sql = "SELECT a.* FROM `art` a LEFT JOIN `tag_rel` t ON a.art_id=t.obj_id AND t.channel='art' WHERE a.is_hide=0 AND t.tag='{$tag}' ORDER BY art_id DESC LIMIT {$limit}";
        $data = DB::query($sql);

        // 输出到视图
        View::render('art_index', ['page_show' => $page_show, 'data' => $data]);
    }

    public function show()
    {
        $art_id = Request::get('id');
        $data = Art::gi()->getInfo(['art_id' => $art_id]);

        // 输出到视图
        View::render('art_show', ['data' => $data]);
    }
}
