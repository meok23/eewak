<?php

/**
 * 评论控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\controller;

use app\front\model\Comment;
use fw\base\Conf;
use fw\tool\Json;
use fw\tool\Request;

class ControllerComment extends Base
{
    public function add()
    {
        $ret = Comment::gi()->add();
        if ($ret['code'] == 0) {
            $comment_id = $ret['data']['comment_id'];
            $info = Comment::gi()->getInfo(['comment_id' => $comment_id]);
            $ret['data'] = $info;
        }

        echo Json::encode($ret);
    }

    public function index()
    {
        // 参数获取
        $channel = Request::get('channel');
        $obj_id = Request::get('obj_id');
        $length = Conf::$all['front_comment_length'];
        $current_page = (int)Request::get('page', null, 1);

        // 条数总计
        $list_count = Comment::gi()->getList([
            'channel'  => $channel,
            'obj_id'   => $obj_id,
            'is_count' => true,
        ]);
        $page_total = ceil($list_count / $length);

        // 查询列表
        $data = Comment::gi()->getList([
            'channel' => $channel,
            'obj_id'  => $obj_id,
            'length'  => $length,
            'page'    => $current_page,
        ]);

        // 输出
        echo Json::encode([
            'code' => 0,
            'msg'  => 'Success.',
            'data' => [
                'data'         => $data,
                'current_page' => $current_page,
                'page_total'   => $page_total,
                'list_count'   => $list_count,
            ],
        ]);
    }
}
