<?php

/**
 * 文章
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\controller;


use app\front\model\Page;
use fw\base\View;
use fw\tool\Request;

class ControllerPage extends Base
{
    public function show()
    {
        $alias = Request::get('alias');
        $data = Page::gi()->getInfo(['alias' => $alias]);

        View::render('page_show', ['data' => $data]);
    }
}
