<?php

/**
 * 文档章节控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\controller;

use app\front\model\Chapter;
use app\front\model\Doc;
use fw\base\View;
use fw\tool\Request;

class ControllerChapter extends Base
{
    public function show()
    {
        $chapter_id = (int)Request::get('id');
        $chapter_info = Chapter::gi()->getInfo(['chapter_id' => $chapter_id]);
        $doc_id = $chapter_info['doc_id'];
        $doc_info = Doc::gi()->getInfo(['doc_id' => $doc_id]);

        // 章节树
        $chapter_rows = Chapter::gi()->getList(['doc_id' => $doc_id]);
        $chapter_tree = Chapter::gi()->ChapterTree($chapter_rows);

        // 输出到视图
        View::render('chapter_show', [
            'data'         => $chapter_info,
            'doc_info'     => $doc_info,
            'chapter_tree' => $chapter_tree,
        ]);
    }
}
