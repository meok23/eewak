<?php

/**
 * 基础控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\front\controller;

use fw\base\Conf;

class Base
{
    public function __construct()
    {
        // 初始化
        $this->__init();

        // 渲染
        $this->_draw();
    }

    public function __init()
    {
        // 这个空方法必须保留，给子类继承
    }

    /**
     * 渲染
     */
    private function _draw()
    {
        // 单页列表
        $page_list = \app\front\model\Page::gi()->getList();

        // 渲染 单页列表
        Conf::$all['page_list'] = $page_list;
    }
}
