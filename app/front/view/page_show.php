<?php

use fw\base\Conf;
use fw\tool\Url;

?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $data['title']; ?> - <?php echo Conf::$all['title']; ?></title>
    <link rel="icon" href="<?php echo Url::root(); ?>favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/normalize.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/front/preset.css">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/github-markdown.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/prism/prism.css">

    <style>
        .master .title {
            margin: 30px 0;
            color: #4A4A4A;
            font-size: 23px;
            font-weight: normal;
            letter-spacing: 1px;
        }
    </style>
</head>
<body>

<?php require Conf::$all['view_path'] . 'base/aside.php'; ?>

<main>
    <article class="master">
        <h2 class="title">
            # <?php echo $data['title']; ?>
        </h2>
        <div class="markdown-body" id="markdown_code"><?php echo $data['content']; ?></div>
    </article>
</main>

<?php require Conf::$all['view_path'] . 'base/footer.php'; ?>

<script src="<?php echo Url::root(); ?>assets/lib/jquery.js"></script>
<script src="<?php echo Url::root(); ?>assets/front/preset.js"></script>

<script src="<?php echo Url::root(); ?>assets/lib/marked.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/prism/prism.js"></script>
<script>

    // markdown 效果
    var markdown_code = $('#markdown_code');
    var mdHtml = marked(markdown_code.text());
    markdown_code.html(mdHtml);

    // 代码高亮
    $('.markdown-body pre code').each(function (index, element) {
        Prism.highlightElement(element);
    });

</script>

</body>
</html>
