<?php

use fw\base\Conf;
use fw\tool\Url;

?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $doc_info['title']; ?> - <?php echo Conf::$all['title']; ?></title>
    <link rel="icon" href="<?php echo Url::root(); ?>favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/normalize.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/front/preset.css">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/github-markdown.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/prism/prism.css">

    <style>
        main {
            overflow-y: hidden;
            margin-top: 24px;
            width: 97%;
        }

        main .cat-tree {
            border-right: solid #e7e7e7 1px;
            margin-right: 20px;
            width: 380px;
            float: left;
        }

        main .cat-tree > .tt {
            margin: 0;
            padding: 7px;
            border-bottom: solid #e7e7e7 1px;
            font-size: 16px;
        }

        main .master {
            padding-left: 400px;
        }

        main .master img {
            max-width: 100%;
        }

        body > footer > .foot-0 {
            width: 97%;
        }
    </style>
</head>
<body>

<main>
    <aside class="cat-tree">
        <div class="tt">
            <b><?php echo $doc_info['title']; ?></b>
        </div>
        <?php echo $chapter_tree; ?>
    </aside>

    <article class="master">
        <h1>
            <?php echo $data['title']; ?>
        </h1>
        <div class="markdown-body" id="markdown_code"><?php echo $data['content']; ?></div>
    </article>
</main>

<?php require Conf::$all['view_path'] . 'base/footer.php'; ?>

<script src="<?php echo Url::root(); ?>assets/lib/jquery.js"></script>
<script src="<?php echo Url::root(); ?>assets/front/preset.js"></script>

<script src="<?php echo Url::root(); ?>assets/lib/marked.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/prism/prism.js"></script>
<script>

    // markdown 效果
    var markdown_code = $('#markdown_code');
    var mdHtml = marked(markdown_code.text());
    markdown_code.html(mdHtml);

    // 代码高亮
    $('.markdown-body pre code').each(function (index, element) {
        Prism.highlightElement(element);
    });

</script>

</body>
</html>
