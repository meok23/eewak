<?php

use fw\base\Conf;
use fw\tool\Url;

?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Keywords" content="<?php echo Conf::$all['seo_keyword']; ?>">
    <meta name="description" content="<?php echo Conf::$all['seo_desc']; ?>">
    <title>文档 - <?php echo Conf::$all['title']; ?></title>
    <link rel="icon" href="<?php echo Url::root(); ?>favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/normalize.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/front/preset.css">
    <style>
        .master {
            padding-top: 34px;
        }

        .tags-list a {
            display: inline-block;
            padding: 1px 4px;
            background-color: #c2c2c2;
            color: #fff;
        }

        .tags-list a:hover {
            color: #f00;
        }
    </style>
</head>
<body>

<?php require Conf::$all['view_path'] . 'base/aside.php'; ?>

<main>
    <article class="master">
        <div class="tags-list" id="tags-list">
            <?php foreach ($data as $fo): ?>
                <p>
                    <?php
                    foreach ($fo as $k => $tag) {
                        if ($k != 0) echo ' -^- ';
                        echo '<a href="' . Url::to('Art/search', ['tag' => $tag]) . '">' . $tag . '</a>';
                    }
                    ?>
                </p>
            <?php endforeach; ?>
        </div>
    </article>
</main>

<?php require Conf::$all['view_path'] . 'base/footer.php'; ?>

<script src="<?php echo Url::root(); ?>assets/lib/jquery.js"></script>
<script src="<?php echo Url::root(); ?>assets/front/preset.js"></script>

</body>
</html>
