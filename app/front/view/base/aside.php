<?php

use fw\tool\Url;
use fw\base\Conf;

?>

<aside class="menu">
    <div class="menu-0">
        <ul>
            <li>
                <a href="<?php echo Url::root(); ?>">首页</a>
            </li>
            <li>
                <a href="<?php echo Url::to('Doc'); ?>">专栏</a>
            </li>
            <li>
                <a href="<?php echo Url::to('Index/tags'); ?>">标签</a>
            </li>
            <?php foreach (Conf::$all['page_list'] as $fo): ?>
                <li>
                    <a href="<?php echo Url::to('Page/show', ['alias' => $fo['alias']]); ?>">
                        <?php echo $fo['title']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <h1>- 名字没想好 ^-^</h1>
    </div>
</aside>
