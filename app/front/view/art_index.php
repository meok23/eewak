<?php

use fw\base\Conf;
use fw\tool\Url;

?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Keywords" content="<?php echo Conf::$all['seo_keyword']; ?>">
    <meta name="description" content="<?php echo Conf::$all['seo_desc']; ?>">
    <title>文章 - <?php echo Conf::$all['title']; ?></title>
    <link rel="icon" href="<?php echo Url::root(); ?>favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/normalize.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/front/preset.css">

    <style>
        .master {
            padding-top: 34px;
        }

        .master > section {
            margin: 0 0 14px 0;
            padding-bottom: 6px;
            border-bottom: solid 1px #f1f1f1;
        }

        .master .title:hover {
            color: #111;
        }

        .master .title > a {
            color: #444;
            font-size: 16px;
            letter-spacing: 1px;
        }

        .master .title > a:hover {
            color: #000;
        }

        .master .widget {
            float: right;
        }

        .master .widget > .tag {
            display: inline-block;
        }

        .master .widget > .tag > a {
            color: #bbb;
            display: inline-block;
            margin: 0 4px;
        }

        .master .widget > .tag > a:hover {
            color: #444;
        }
    </style>
</head>
<body>

<?php require Conf::$all['view_path'] . 'base/aside.php'; ?>

<main>
    <article class="master">
        <?php foreach ($data as $fo): ?>
            <section>
                <div class="widget">
                    <div class="tag">
                        <?php
                        if (!empty($fo['tags'])) {
                            $tag_arr = explode(',', $fo['tags']);
                            foreach ($tag_arr as $tag) {
                                echo '<a href="' . Url::to('Art/search', ['tag' => $tag]) . '">' . $tag . '</a>';
                            }
                            echo '<span>&nbsp;^</span>';
                        }
                        ?>
                    </div>
                    <?php echo date('Y-m-d', $fo['create_time']); ?>
                </div>
                <div class="title">
                    <a href="<?php echo Url::to('Art/show', ['id' => $fo['art_id']]); ?>">
                        <?php echo $fo['title']; ?>
                    </a>
                </div>
            </section>
        <?php endforeach; ?>
        <footer class="page clear">
            <?php echo $page_show; ?>
        </footer>
    </article>
</main>

<?php require Conf::$all['view_path'] . 'base/footer.php'; ?>

<script src="<?php echo Url::root(); ?>assets/lib/jquery.js"></script>
<script src="<?php echo Url::root(); ?>assets/front/preset.js"></script>

</body>
</html>
