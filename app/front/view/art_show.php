<?php

use fw\base\Conf;
use fw\tool\Url;

?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $data['title']; ?> - <?php echo Conf::$all['title']; ?></title>
    <link rel="icon" href="<?php echo Url::root(); ?>favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/normalize.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/front/preset.css">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/github-markdown.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/prism/prism.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/comment/comment.css">

    <style>
        .master .title {
            margin: 30px 0;
            color: #4A4A4A;
            font-size: 23px;
            font-weight: normal;
            letter-spacing: 1px;
        }

        .master .widget {
            margin: 14px 0;
            color: #bbb;
        }

        .master .widget > .tag {
            display: inline-block;
        }

        .master .widget > .tag > a {
            font-style: italic;
            color: #bbb;
            display: inline-block;
            margin: 0 4px;
        }

        .master .markdown-body {
            margin-top: 43px;
        }

        .comment .comment-show {
            margin-top: 43px;
            padding-top: 4px;
            border-top: solid 1px #DDECF1;
        }
    </style>
</head>
<body>

<?php require Conf::$all['view_path'] . 'base/aside.php'; ?>

<main>
    <article class="master">
        <h2 class="title">
            # <?php echo $data['title']; ?>
        </h2>
        <div class="widget">
            <?php
            echo date('Y-m-d', $data['create_time']);
            ?>
            <div class="tag">
                <?php
                if (!empty($data['tags'])) {
                    echo '<span>&nbsp;^&nbsp;</span>';
                    $tag_arr = explode(',', $data['tags']);
                    foreach ($tag_arr as $tag) {
                        echo '<a href="' . Url::to('Art/search', ['tag' => $tag]) . '">' . $tag . '</a>';
                    }
                }
                ?>
            </div>
        </div>
        <div class="markdown-body" id="markdown_code"><?php echo $data['content']; ?></div>
    </article>

    <?php if (Conf::$all['art_is_comment'] === true): ?>
        <section class="comment">
            <div class="comment-show" id="comment_show"></div>
            <div class="comment-input">
                <p>我有话要说</p>
                <form id="comment_form" role="form">
                    <input name="channel" type="hidden" value="art">
                    <input name="obj_id" type="hidden" value="<?php echo $data['art_id']; ?>">
                    <input id="to_comment_id" name="to_comment_id" type="hidden" value="0">
                    <dl>
                        <dt>
                            评论内容
                            <smal style="color: #ccc;">200字以内</smal>
                        </dt>
                        <dd>
                            <div class="quote-preview" id="quote_preview"></div>
                            <textarea name="content" rows="4" data-required_notice="请输入内容"
                                      data-strmax_notice="不得超过200个字" data-strmax_value="200"></textarea>
                            <span class="error"></span>
                        </dd>
                        <dt>
                            称呼
                        </dt>
                        <dd>
                            <input name="nickname" type="text" data-required_notice="称呼必填"> &laquo;- 必填
                            <span class="error"></span>
                        </dd>
                        <dt>E-mail</dt>
                        <dd>
                            <input name="email" type="text"> &laquo;- 填的话，有回复会通知你
                        </dd>
                        <dt>个人网址</dt>
                        <dd>
                            <input name="link" type="text"> &laquo;- 你随意
                        </dd>
                        <dt></dt>
                        <dd style="padding-top: 20px;">
                            <a class="btn" href="javascript:;" data-url="<?php echo Url::to('Comment/add'); ?>"
                               onclick="comment.commentSubmit(this);">
                                提交
                            </a> &laquo;- 确定无误则点击提交，无法修改的哦
                            <p class="error" id="comment_form_notice"></p>
                        </dd>
                    </dl>
                </form>
            </div>
        </section>
    <?php endif; ?>
</main>

<?php require Conf::$all['view_path'] . 'base/footer.php'; ?>

<script src="<?php echo Url::root(); ?>assets/lib/jquery.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/ext.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/cookie.js"></script>
<script src="<?php echo Url::root(); ?>assets/front/preset.js"></script>

<script src="<?php echo Url::root(); ?>assets/lib/comment/comment.js"></script>
<script>
    $(function () {

        <?php if (Conf::$all['art_is_comment'] === true): ?>
        // 构建评论
        comment.build({
            api: "<?php echo Url::to('Comment/index', ['channel' => 'art', 'obj_id' => $data['art_id']])?>"
        });
        <?php endif; ?>
    });
</script>

<script src="<?php echo Url::root(); ?>assets/lib/marked.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/prism/prism.js"></script>
<script>

    // markdown 效果
    var markdown_code = $('#markdown_code');
    var mdHtml = marked(markdown_code.text());
    markdown_code.html(mdHtml);

    $(function () {
        // 代码高亮
        $('.markdown-body pre code').each(function (index, element) {
            Prism.highlightElement(element);
        });
    });

</script>

</body>
</html>
