<?php

/**
 * 文档模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-06
 */

namespace app\admin\model;

use app\common\logic\Response;
use fw\base\Instance;
use fw\db\DB;
use fw\tool\Request;

class Doc extends \app\common\model\Doc
{
    use Instance;

    /**
     * 新增一条数据
     *
     * @return array
     */
    public function add()
    {
        // 参数获取
        $title = Request::post('title', 'trim');
        $brief = Request::post('brief', 'trim');
        $manager_id = (int)Request::post('manager_id');

        $is_hide = Request::post('is_hide', function ($pa) {
            return in_array($pa, [0, 1]) ? $pa : 0;
        });

        $types = Request::post('types', 'trim', 'doc');
        $url = Request::post('url', 'trim');

        // 参数校验
        if (empty($title) || $manager_id < 1) {
            return Response::to([400, 'Parameter error.']);
        }

        // 数据准备
        $data = [
            'title'       => $title,
            'brief'       => $brief,
            'manager_id'  => $manager_id,
            'is_hide'     => (int)$is_hide,
            'create_time' => time(),
            'update_time' => 0,
            'types'       => $types,
            'url'         => $url,
        ];

        // 操作数据库
        $ret = DB::table('doc')->insert($data);
        if ($ret == false) {
            return Response::log([500, 'Insert fail.', $data], Request::post());
        }
        $doc_id = DB::$dbh->lastInsertId();

        return Response::to([0, 'Success.', ['doc_id' => $doc_id]]);
    }

    /**
     * 更新一条数据
     *
     * @return array
     */
    public function update()
    {
        // 参数获取
        $doc_id = (int)Request::post('doc_id');
        $title = Request::post('title', 'trim');
        $brief = Request::post('brief', 'trim');
        $manager_id = (int)Request::post('manager_id');

        $is_hide = Request::post('is_hide', function ($pa) {
            return in_array($pa, [0, 1]) ? $pa : 0;
        });

        $types = Request::post('types', 'trim', 'doc');
        $url = Request::post('url', 'trim');

        // 参数校验
        if ($doc_id < 1 || $manager_id < 1) {
            return Response::to([400, 'Parameter error.']);
        }

        // 数据准备
        $data = [];
        if (!empty($title)) {
            $data['title'] = $title;
        }
        if (!empty($brief)) {
            $data['brief'] = $brief;
        }
        if ($manager_id > 0) {
            $data['manager_id'] = $manager_id;
        }
        if (!is_null($is_hide)) {
            $data['is_hide'] = $is_hide;
        }
        if (!empty($types)) {
            $data['types'] = $types;
        }
        if (!empty($url)) {
            $data['url'] = $url;
        }

        if (empty($data)) {
            return Response::to([400, 'Parameter error, data empty.']);
        }

        $data['update_time'] = time();
        $affected_rows = DB::table('doc')->where('doc_id=:doc_id', [':doc_id' => $doc_id])->update($data);
        if ($affected_rows < 1) {
            return Response::to([200, 'Without any modification.', $data]);
        }

        return Response::to([0, 'Success.']);
    }

    /**
     * 删除数据
     *
     * @param array $args
     *
     * @return array
     */
    public function del($args)
    {
        // 参数获取
        $doc_id = (int)Request::input($args, 'doc_id');
        $op_user = (int)Request::input($args, 'op_user');

        // 参数校验
        if ($doc_id < 1 || $op_user < 1) {
            return Response::to([400, 'Parameter error.']);
        }

        // 查询信息
        $info = DB::table('doc')->where('doc_id=:doc_id', [':doc_id' => $doc_id])->selectRow();
        if (empty($info)) {
            return Response::to([404, 'Record does not exists.']);
        }

        // 放入回收站
        Recy::gi()->add('doc', $info, $op_user);

        // 删除数据
        $affected_rows = DB::table('doc')->where('doc_id=:doc_id', [':doc_id' => $doc_id])->delete();
        if ($affected_rows < 1) {
            return Response::log([500, 'Insert fail.'], $args);
        }

        return Response::to([0, 'Success.']);
    }
}
