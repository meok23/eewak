<?php

/**
 * 文章模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-05
 */

namespace app\admin\model;

use app\common\logic\Response;
use fw\base\Instance;
use fw\db\DB;
use fw\tool\Request;
use fw\tool\Str;

class Page extends \app\common\model\Page
{
    use Instance;

    /**
     * 新增一条数据
     *
     * @return array
     */
    public function add()
    {
        // 参数获取
        $alias = Request::post('alias', 'trim');
        $title = Request::post('title', 'trim');
        $content = Request::post('content', 'trim');
        $manager_id = (int)Request::post('manager_id');
        $manager_id = $manager_id > 0 ? $manager_id : 0;
        $tags = Request::post('tags', 'trim');

        $is_hide = Request::post('is_hide', function ($pa) {
            return in_array($pa, [0, 1]) ? $pa : 0;
        });

        // 参数校验
        if (empty($title) || $manager_id < 1) {
            return Response::to([400, 'Parameter error.']);
        }

        // 数据准备
        $brief = Str::stripMd($content, 200);
        $data = [
            'title'       => $title,
            'content'     => $content,
            'manager_id'  => $manager_id,
            'create_time' => time(),
            'update_time' => 0,
            'tags'        => $tags,
            'brief'       => $brief,
            'is_hide'     => (int)$is_hide,
        ];

        // 查询重复
        $ret = DB::table($this->_tbl)->where('alias=:alias', [':alias' => $alias])->count();
        if ($ret > 0) {
            return Response::to([400, 'The alias already exist.']);
        }
        $data['alias'] = $alias;

        // 操作数据库
        $ret = DB::table($this->_tbl)->insert($data);
        if ($ret == false) {
            return Response::log([500, 'Insert fail.', $data], Request::post());
        }
        $page_id = DB::$dbh->lastInsertId();

        // 更新标签关系
        $tag_arr = explode(',', $tags);
        foreach ($tag_arr as $tag) {
            TagRel::gi()->add(['tag' => $tag, 'obj_id' => $page_id, 'channel' => 'page']);
        }

        return Response::to([0, 'Success.', ['page_id' => $page_id]]);
    }

    /**
     * 更新一条数据
     *
     * @return array
     */
    public function update()
    {
        // 参数获取
        $page_id = (int)Request::post('page_id');
        $alias = Request::post('alias', 'trim');
        $title = Request::post('title', 'trim');
        $content = Request::post('content', 'trim');
        $manager_id = (int)Request::post('manager_id');
        $manager_id = $manager_id > 0 ? $manager_id : 0;
        $tags = Request::post('tags', 'trim');

        $is_hide = Request::post('is_hide', function ($pa) {
            return in_array($pa, [0, 1]) ? $pa : 0;
        });

        // 参数校验
        if ($page_id < 1 || $manager_id < 1) {
            return Response::to([400, 'Parameter error.']);
        }

        // 数据准备
        $data = [];
        if (!empty($alias)) {
            $data['alias'] = $alias;
        }
        if (!empty($title)) {
            $data['title'] = $title;
        }
        if (!empty($content)) {
            $data['content'] = $content;

            $brief = Str::stripMd($content, 200);
            $data['brief'] = $brief;
        }
        if ($manager_id > 0) {
            $data['manager_id'] = $manager_id;
        }
        if (!empty($tags)) {
            $data['tags'] = $tags;
        }
        if (!is_null($is_hide)) {
            $data['is_hide'] = $is_hide;
        }

        if (empty($data)) {
            return Response::to([400, 'Parameter error, data empty.']);
        }

        $data['update_time'] = time();
        $affected_rows = DB::table($this->_tbl)->where('page_id=:page_id', [':page_id' => $page_id])->update($data);
        if ($affected_rows < 1) {
            return Response::to([200, 'Without any modification.', $data]);
        } else {
            // 更新标签关系
            if (!empty($tags)) {
                TagRel::gi()->del(['obj_id' => $page_id, 'channel' => 'page']);
                $tag_arr = explode(',', $tags);
                foreach ($tag_arr as $tag) {
                    TagRel::gi()->add(['tag' => $tag, 'obj_id' => $page_id, 'channel' => 'page']);
                }
            }
        }

        return Response::to([0, 'Success.']);
    }

    /**
     * 删除数据
     *
     * @param array $args
     *
     * @return array
     */
    public function del($args)
    {
        // 参数获取
        $page_id = (int)Request::input($args, 'page_id');
        $op_user = (int)Request::input($args, 'op_user');

        // 参数校验
        if ($page_id < 1 || $op_user < 1) {
            return Response::to([400, 'Parameter error.']);
        }

        // 查询信息
        $info = DB::table($this->_tbl)->where('page_id=:page_id', [':page_id' => $page_id])->selectRow();
        if (empty($info)) {
            return Response::log([404, 'Record does not exists.', ['page_id' => $page_id]]);
        }

        // 放入回收站
        Recy::gi()->add('page', $info, $op_user);

        // 删除
        $affected_rows = DB::table($this->_tbl)->where('page_id=:page_id', [':page_id' => $page_id])->delete();
        if ($affected_rows < 1) {
            return Response::log([500, 'Delete fail.', ['page_id' => $page_id]], $args);
        }

        // 删除标签关系
        TagRel::gi()->del(['obj_id' => $page_id, 'channel' => 'page']);

        return Response::to([0, 'Success.']);
    }
}
