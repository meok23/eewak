<?php

/**
 * 评论模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace app\admin\model;

use app\common\logic\Response;
use fw\base\Instance;
use fw\db\DB;
use fw\tool\Request;

class Comment extends \app\common\model\Comment
{
    use Instance;

    /**
     * 评论审阅
     *
     * @return array
     */
    public function check()
    {
        // 参数获取
        $comment_ids = Request::post('comment_ids', 'trim');
        $status = Request::post('status');

        // 参数校验
        if (empty($comment_ids) || !in_array($status, [0, 1])) {
            return Response::to([400, 'Parameter error.']);
        }

        // 操作数据库
        $affected_rows = DB::table('comment')->where('comment_id IN(' . $comment_ids . ')')->update(['status' => $status]);
        if ($affected_rows < 1) {
            return Response::log([500, 'Update fail.'], Request::post());
        }

        return Response::to([0, 'Success.', ['affected_rows' => $affected_rows]]);
    }

    /**
     * 删除数据
     *
     * @param array $args
     *
     * @return array
     */
    public function del($args)
    {
        // 参数获取
        $comment_id = (int)Request::input($args, 'comment_id');
        $op_user = (int)Request::input($args, 'op_user');

        // 参数校验
        if ($comment_id < 1 || $op_user < 1) {
            return Response::to([400, 'Parameter error.']);
        }

        // 查询信息
        $info = DB::table('comment')->where('comment_id=:comment_id', [':comment_id' => $comment_id])->selectRow();
        if (empty($info)) {
            return Response::log([404, 'Parameter error.'], $args);
        }

        // 放入回收站
        $data = [
            'channel'  => 'comment',
            'content'  => serialize($info),
            'del_time' => time(),
            'op_user'  => $op_user,
        ];
        DB::table('recy')->insert($data);

        // 删除数据
        $affected_rows = DB::table('comment')->where('comment_id=:comment_id', [':comment_id' => $comment_id])->delete();
        if ($affected_rows < 1) {
            return Response::log([500, 'Delete fail.'], $args);
        }

        return Response::to([0, 'Success.']);
    }
}
