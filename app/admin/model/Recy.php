<?php

/**
 * 回收站模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-06
 */

namespace app\admin\model;

use fw\base\Instance;
use fw\db\DB;

class Recy
{
    use Instance;

    public function add($channel, $content_raw, $op_user)
    {
        // 放入回收站
        $data = [
            'channel'  => $channel,
            'content'  => serialize($content_raw),
            'del_time' => time(),
            'op_user'  => $op_user,
        ];
        DB::table('recy')->insert($data);
    }
}
