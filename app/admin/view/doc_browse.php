@extends('base/layout_1')

@section('contents')
<main>
    <section>
        <header>
            <div>
                <a class="btn" href="<?php echo Url::to('Doc/add'); ?>">新增</a>
            </div>
        </header>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th width="240">
                        文档名
                    </th>
                    <th width="54">
                        类型
                    </th>
                    <th>##</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $fo): ?>
                    <tr>
                        <td><?php echo $fo['title']; ?></td>
                        <td><?php echo $fo['types']; ?></td>
                        <td>
                            <a href="<?php echo Url::to('Doc/update', ['doc_id' => $fo['doc_id']]); ?>">设置</a>
                            <?php if ($fo['types'] === 'doc'): ?>
                                <a href="<?php echo Url::to('Chapter/update', ['doc_id' => $fo['doc_id']]); ?>">编辑</a>
                                <a href="<?php echo Url::to('Doc/show', ['id' => $fo['doc_id']], Conf::$all['front_script']); ?>"
                                   target="_blank">浏览</a>
                            <?php else: ?>
                                <a href="<?php echo $fo['url']; ?>" target="_blank">浏览</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <footer class="page">
            <?php echo $page_show; ?>
        </footer>
    </section>
</main>
endsection
