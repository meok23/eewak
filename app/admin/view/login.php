<?php

use fw\base\Conf;
use fw\tool\Url;

?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>登录 - <?php echo Conf::$all['title']; ?></title>
    <link rel="icon" href="<?php echo Url::root(); ?>assets/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/normalize.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/admin/preset.css">

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        dl.login .form {
            width: 100%;
        }

        dl.login {
            width: 270px;
            margin: 0 auto;
            padding: 40px 0 0 0;
        }

        dl.login > dt {
            margin: 0;
            padding: 5px 0 5px 0;
        }

        dl.login > dd {
            margin: 0 0 14px 0;
            padding: 0;
        }

        dl.login > dd > p:after {
            content: ".";
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
        }

        dl.login > dd > p > label > a {
            background-color: #64889B;
        }
    </style>
</head>
<body>

<main>
    <form method="post" action="<?php echo Url::to('Common/doLogin'); ?>">
        <dl class="login">
            <dt>邮箱</dt>
            <dd>
                <input class="form" name="email" type="input">
            </dd>
            <dt>密码</dt>
            <dd>
                <input class="form" name="password" type="password">
            </dd>
            <dd>
                <label>
                    <input name="keep" type="checkbox" value="7">
                    记住一周
                </label>
            </dd>
            <dd>
                <p>
                    <label class="float-right">
                        <a class="btn" href="">忘记密码 ^_^?</a>
                    </label>
                    <label>
                        <button class="btn">登录</button>
                    </label>
                </p>
            </dd>
        </dl>
    </form>
</main>

<script src="<?php echo Url::root(); ?>assets/lib/jquery.js"></script>
<script src="<?php echo Url::root(); ?>assets/admin/preset.js"></script>

</body>
</html>
