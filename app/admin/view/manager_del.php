@extends('base/layout_1')

@section('contents')
<main>
    <section>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        <input id="check_all" type="checkbox">
                        manager_id
                    </th>
                    <th>用户名</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $fo): ?>
                    <tr>
                        <td>
                            <input data-checked="true" value="<?php echo $fo['manager_id']; ?>" type="checkbox"
                                   checked="checked">
                            <?php echo $fo['manager_id']; ?>
                        </td>
                        <td><?php echo $fo['name']; ?></td>
                        <td><?php echo $fo['email']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <p class="notice">
                删除不可恢复，建议禁用而非删除
            </p>
        </div>
        <div class="Confirm-del" id="ajax_form">
            <span>管理密码</span>
            <input class="form" name="manager_password" data-required_notice="请输入密码。" type="password">
            <a class="btn" data-url="<?php echo Url::to('Manager/del'); ?>" href="javascript:;"
               onclick="ajaxDel(this);">确定删除</a>
            <a class="btn btn-back" href="javascript:history.go(-1);">返回列表</a>
            <p class="error" id="form_notice"></p>
        </div>
    </section>
</main>
endsection

@section('head')
<style>
    .notice {
        color: #00f;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>
endsection
