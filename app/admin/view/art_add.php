@extends('base/layout_1')

@section('contents')
<main>
    <form id="ajax_form" role="form" onsubmit="return false">

        <div class="input-panel float-left" style="padding-right: 29px;">
            <dl>
                <dt>标题</dt>
                <dd>
                    <input class="form form-large form-title" name="title" data-required_notice="请输入标题。"
                           type="text">
                    <p class="error"></p>
                </dd>
                <dt>内容</dt>
                <dd>

                    <!--编辑器主体-->
                    <?php require Conf::$all['view_path'] . 'base/edit_body.php'; ?>
                </dd>
            </dl>
        </div>

        <div class="input-panel float-left">
            <dl>
                <dt>标签</dt>
                <dd>
                    <textarea class="form form-widget" name="tags" rows="1" placeholder="多个标签使用[英文逗号]分隔"></textarea>
                </dd>
                <dt></dt>
                <dd>
                    <label style="margin-right: 20px;">
                        显示：<input type="radio" name="is_hide" value="0" checked="checked">
                    </label>
                    <label>
                        隐藏：<input type="radio" name="is_hide" value="1">
                    </label>
                </dd>
            </dl>
        </div>

        <div class="btn-bar clear">
            <button class="btn"
                    data-url="<?php echo Url::to('Art/add'); ?>"
                    data-jump="back"
                    type="button"
                    onclick="ajaxSubmit(this);"> 提交
            </button>
            <a class="btn btn-back" href="javascript:history.go(-1);">返回列表</a>
            <p id="form_notice"></p>
        </div>
    </form>
</main>
endsection

@section('head')
<!--编辑器样式-->
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/eewakeditor/eewakeditor.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/github-markdown.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/prism/prism.css">

<style>
    .form-title,
    .eewakeditor {
        width: 800px;
    }

    .eewakeditor textarea {
        height: 230px;
    }

    .form-widget {
        width: 300px;
    }

    .form-cat > p {
        margin: 4px;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>

<!--编辑器脚本-->
<?php require Conf::$all['view_path'] . 'base/edit_script.php'; ?>
endsection
