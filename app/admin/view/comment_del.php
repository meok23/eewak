@extends('base/layout_1')

@section('contents')
<main>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th>
                    <input id="check_all" type="checkbox">
                    comment_id
                </th>
                <th>内容</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $fo): ?>
                <tr>
                    <td>
                        <input data-checked="true" type="checkbox" value="<?php echo $fo['comment_id']; ?>"
                               checked="checked">
                        <?php echo $fo['comment_id']; ?>
                    </td>
                    <td>
                        <div style="color: #666;">
                            <?php echo $fo['quote']; ?>
                        </div>
                        <div>
                            <?php echo $fo['content']; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="notice">
            删除不可恢复！
        </p>
    </div>
    <div class="Confirm-del" id="ajax_form">
        <span>管理密码</span>
        <input class="form" name="manager_password" data-required_notice="请输入密码。" type="password">
        <a href="javascript:;" class="btn" data-url="<?php echo Url::to('Comment/del'); ?>"
           onclick="ajaxDel(this);">确定删除</a>
        <a href="javascript:history.go(-1);" class="btn btn-back">返回列表</a>
        <p class="error" id="form_notice"></p>
    </div>
</main>
endsection

@section('head')
<style>
    .notice {
        color: #00f;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>
endsection
