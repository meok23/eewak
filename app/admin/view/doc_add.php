@extends('base/layout_1')

@section('contents')
<main>
    <form id="ajax_form" role="form" onsubmit="return false">
        <div class="input-panel">
            <dl>
                <dt>标题</dt>
                <dd>
                    <input class="form" name="title" data-required_notice="请输入标题。" type="text">
                    <p class="error"></p>
                </dd>
                <dt>简介</dt>
                <dd>
                    <textarea class="form" name="brief"></textarea>
                </dd>
            </dl>

            <div class="item">
                <p>类型</p>
                <div>
                    <label style="margin-right: 20px;">
                        <input type="radio" name="types" value="doc" checked="checked"> 文档[doc]
                    </label>
                    <label>
                        <input type="radio" name="types" value="url"> 外链[url]
                    </label>
                </div>
            </div>
            <div class="item">
                <p>链接</p>
                <div>
                    <input class="form" name="url" type="text">
                </div>
            </div>
        </div>

        <div class="btn-bar">
            <button class="btn"
                    data-url="<?php echo Url::to('Doc/add'); ?>"
                    data-jump="back"
                    type="button"
                    onclick="ajaxSubmit(this);"> 提交
            </button>
            <a class="btn btn-back" href="javascript:history.go(-1);">返回列表</a>
            <p id="form_notice"></p>
        </div>
    </form>
</main>
endsection

@section('head')
<style>
    .input-panel {
        padding-bottom: 30px;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>
endsection
