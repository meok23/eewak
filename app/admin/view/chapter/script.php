<?php

use fw\tool\Url;

?>

<script src="<?php echo Url::root(); ?>assets/lib/sortable.mul.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/warn.js"></script>
<script>

    // 拖动排序
    $('#sortable').sortable({
        success: function (res) {
            var sId = [];
            $(res).parent().children().each(function (index) {
                sId.push($(this).data('id'));
            });

            var url = "<?php echo Url::to('Chapter/setSort'); ?>";

            $.post(url, {chapter_ids: sId.toString()}, function (res) {
                if (res.code !== 0) {
                    $.warn({msg: res.msg});
                }
            }, 'json');
        }
    });

    // 删除章节
    function del(item) {
        var chapter_id = $(item).data('id');

        if (true === confirm('确认删除？')) {
            var url = '<?php echo Url::to('Chapter/del'); ?>';
            $.post(url, {id: chapter_id}, function (res) {
                if (res.code !== 0) {
                    $.warn({msg: res.msg});
                } else {
                    $(item).closest('li').remove();
                }
            }, 'json');
        }
    }

</script>
