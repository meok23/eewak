@extends('base/layout_1')

@section('contents')
<main>
    <form id="ajax_form" role="form" onsubmit="return false">
        <input name="manager_id" type="hidden" value="<?php echo $data['manager_id'] ?>">
        <div class="input-panel">
            <dl>
                <dt>
                    用户名
                    <small>(用户名随意)</small>
                </dt>
                <dd>
                    <input class="form" name="name" type="text" value="<?php echo $data['name']; ?>"
                           data-required_notice="请输入用户名。">
                    <p class="error"></p>
                </dd>
                <dt>
                    邮箱
                    <small>(邮箱唯一，用于登录、找回密码等安全操作)</small>
                </dt>
                <dd>
                    <input class="form" name="email" type="text" value="<?php echo $data['email']; ?>"
                           data-required_notice="请输入邮箱。">
                    <p class="error"></p>
                </dd>
                <dt>
                    密码
                    <small>(密码不能小于6位)</small>
                    <small>(留空或全部为 "0" 则不修改)</small>
                </dt>
                <dd>
                    <input class="form" name="password" type="password" value="000000">
                </dd>
                <dt>
                    确认密码
                </dt>
                <dd>
                    <input class="form" name="re_password" type="password" value="000000">
                </dd>
            </dl>
        </div>
        <div class="btn-bar">
            <button class="btn"
                    data-url="<?php echo Url::to('Manager/updateMyzone'); ?>"
                    data-jump="back"
                    type="button"
                    onclick="ajaxSubmit(this);"> 提交
            </button>
            <p class="error" id="form_notice"></p>
        </div>
    </form>
</main>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>
endsection
