@extends('base/layout_1')

@section('contents')
<main>
    <aside class="cat-tree">
        <div class="tt">
            <b>《<?php echo $doc_info['title']; ?>》</b>
            <a class="float-right"
               href="<?php echo Url::to('Chapter/add', ['doc_id' => $doc_info['doc_id']]); ?>">[新增 +]</a>
        </div>
        <div class="cc">
            <?php echo $chapter_tree; ?>
        </div>
    </aside>

    <section>
        <?php if (!empty($data)): ?>
            <form id="ajax_form" role="form" onsubmit="return false">
                <input name="chapter_id" type="hidden" value="<?php echo $data['chapter_id']; ?>">

                <div class="input-panel">
                    <dl>
                        <dt>
                            上级目录
                        </dt>
                        <dd>
                            <select class="form" name="parent_id">
                                <option value="0">--顶端，没有上级--</option>
                                <?php
                                foreach ($select_tree as $fo) {
                                    echo '<option value="' . $fo['chapter_id'] . '"';
                                    if ($data['parent_id'] == $fo['chapter_id']) {
                                        echo ' selected="selected"';
                                    }
                                    if (-1 == $fo['chapter_id']) {
                                        echo ' disabled="disabled"';
                                        echo ' style="color:#ccc"';
                                    }
                                    echo '>' . trim($fo['title'], '/') . '</option>';
                                }
                                ?>
                            </select>
                        </dd>
                        <dt>标题</dt>
                        <dd>
                            <input class="form form-large form-title" name="title" data-required_notice="请输入标题。"
                                   type="text"
                                   value="<?php echo $data['title']; ?>">
                            <p class="error"></p>
                        </dd>
                    </dl>

                    <?php if ($data['types'] === 'content'): ?>
                        <!--编辑器主体-->
                        <?php require Conf::$all['view_path'] . 'base/edit_body.php'; ?>
                    <?php endif; ?>
                </div>

                <div class="btn-bar clear">
                    <button class="btn"
                            data-url="<?php echo Url::to('Chapter/update', ['doc_id' => $doc_info['doc_id']]); ?>"
                            data-jump="refresh"
                            type="button"
                            onclick="ajaxSubmit(this);"> 提交
                    </button>
                    <a class="btn btn-back" href="<?php echo Url::to('Doc/browse'); ?>">返回文档集</a>
                    <p id="form_notice"></p>
                </div>
            </form>
        <?php else: ?>
            <div class="notice_box">
                <h1 class="emoji"> ←_← </h1>
                还没有章节，
                <a href="<?php echo Url::to('Chapter/add', ['doc_id' => $doc_info['doc_id']]); ?>">立即新增？</a>
            </div>
        <?php endif; ?>
    </section>
</main>
endsection

@section('head')

<!--编辑器样式-->
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/eewakeditor/eewakeditor.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/github-markdown.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/prism/prism.css">

<style>
    main > section {
        margin-left: 350px;
    }

    .cat-tree {
        width: 330px;
    }

    .form-title,
    .eewakeditor {
        width: 800px;
    }

    .eewakeditor textarea {
        height: 230px;
    }

    .notice_box {
        background-color: #F6F6F6;
        border: dashed 1px #cccccc;
        padding: 29px 64px;
        float: left;
    }

    .notice_box .emoji {
        margin: 0 0 14px 0;
    }

    /* 拖动排序 */

    #sortable {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    #sortable li.sortable-target {
        border-bottom: 1px dashed #f00;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>

<?php require Conf::$all['view_path'] . 'chapter/script.php'; ?>

<!--编辑器脚本-->
<?php require Conf::$all['view_path'] . 'base/edit_script.php'; ?>
endsection
