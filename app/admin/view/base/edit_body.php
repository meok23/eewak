<div class="eewakeditor" id="eewakeditor">
    <div class="eewakeditor-tool">
        <i class="bold" data-role="bold" title="加粗">B</i>
        <i class="italic" data-role="italic" title="斜体">italic</i>
        <i title="二级标题" data-role="h2">h2</i>
        <span>|</span>
        <i class="quote" data-role="quote" title="引用"></i>
        <i class="link" data-role="link" title="链接"></i>
        <i class="code" data-role="code" title="代码"></i>
        <i class="img">
            <input id="eewakeditor_file" name="img" type="file" title="图片">
        </i>

        <i class="preview" data-role="preview" title="预览">预览</i>
        <i class="write" data-role="write" title="撰写">撰写</i>
        <i class="full_screen" data-role="full_screen" title="全屏">全屏</i>
        <i class="normal_screen" data-role="normal_screen" title="恢复">恢复</i>
    </div>
    <textarea class="eewakeditor-content"
              name="content"><?php if (!empty($data['content'])) echo $data['content']; ?></textarea>
    <div class="preview_box markdown-body" data-role="preview_box"></div>
</div>
