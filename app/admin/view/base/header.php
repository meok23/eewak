<?php

use fw\base\Conf;
use fw\tool\Url;

?>

<header>
    <div class="member float-right">
        <a href="<?php echo Url::to('', [], Conf::$all['front_script']); ?>" target="_blank">前台</a>
        <span>|</span>
        <a href="<?php echo Url::to('Manager/myzone'); ?>">
            <?php echo Conf::$all['render']['base_username']; ?>
        </a>
        <span>|</span>
        <a href="<?php echo Url::to('Common/logout'); ?>">登出</a>
    </div>
</header>

<nav>
    <ul class="menu">
        <?php foreach (Conf::$all['admin_menu'] as $menu_1): ?>
            <li>
                <div>
                    <span><?php echo $menu_1['title']; ?></span>
                    <i>o</i>
                </div>
                <ul>
                    <?php foreach ($menu_1['_child'] as $menu_2): ?>
                        <li>
                            <a href="<?php echo Url::to($menu_2['url']); ?>"><?php echo $menu_2['title']; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>
