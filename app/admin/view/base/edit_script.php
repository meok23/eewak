<?php

use fw\tool\Url;

?>

<script src="<?php echo Url::root(); ?>assets/lib/eewakeditor/eewakeditor.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/marked.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/upload.js"></script>
<script src="<?php echo Url::root(); ?>assets/lib/prism/prism.js"></script>
<script>
    $(function () {

        // 构建编辑器
        var editor = eewakeditor.build('eewakeditor');

        // 图片上传
        $('#eewakeditor_file').on('change', function () {
            $.upload({
                url: '<?php echo Url::to('Common/uploadImg'); ?>',
                data: null,
                fileId: $(this).attr('id'),
                success: function (res) {
                    var img = '![alt](' + res.data.src + ')';
                    eewakeditor.replace(img);
                }
            });
        });

        // 效果预览
        var textarea = editor.find('textarea');
        var mdPreview = function () {
            var mdHtml = marked(textarea.val());
            editor.find('[data-role=preview_box]').html(mdHtml);

            // 代码高亮
            $('.markdown-body pre code').each(function (index, element) {
                Prism.highlightElement(element);
            });
        };
        textarea.on('input propertychange', function () {
            mdPreview();
        });
        mdPreview();
    });
</script>
