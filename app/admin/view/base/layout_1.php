<?php

use fw\base\Conf;
use fw\base\View;
use fw\tool\Url;

?>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo Conf::$all['title']; ?></title>
    <link rel="icon" href="<?php echo Url::root(); ?>favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/normalize.css">
    <link rel="stylesheet" href="<?php echo Url::root(); ?>assets/admin/preset.css">

    @yield('head')
</head>
<body>

<?php require Conf::$all['view_path'] . 'base/header.php'; ?>

@yield('contents')

<script src="<?php echo Url::root(); ?>assets/lib/jquery.js"></script>
<script src="<?php echo Url::root(); ?>assets/admin/preset.js"></script>

@yield('foot')
</body>
</html>
