@extends('base/layout_1')

@section('contents')
<main>
    <form id="ajax_form" role="form" onsubmit="return false">
        <div class="input-panel">
            <dl>
                <dt>
                    用户名
                    <small>(用户名随意)</small>
                </dt>
                <dd>
                    <input class="form" name="name" data-required_notice="请输入用户名。" type="text">
                    <p class="error"></p>
                </dd>
                <dt>
                    邮箱
                    <small>(邮箱唯一，用于登录、找回密码等安全操作)</small>
                </dt>
                <dd>
                    <input class="form" name="email" data-required_notice="请输入邮箱。" type="text">
                    <p class="error"></p>
                </dd>
                <dt>
                    密码
                    <small>(密码不能小于6位，不能全部为 "0")</small>
                </dt>
                <dd>
                    <input class="form" name="password" data-required_notice="请输入密码。" type="password">
                    <p class="error"></p>
                </dd>
                <dt>
                    确认密码
                </dt>
                <dd>
                    <input class="form" name="re_password" data-required_notice="请再次输入密码。" type="password">
                    <p class="error"></p>
                </dd>
                <dt>
                    开启或禁用
                </dt>
                <dd>
                    <label>
                        <input name="status" type="radio" value="1" checked="checked">
                        开启
                    </label>
                    <label>
                        <input name="status" type="radio" value="0">
                        禁用
                    </label>
                </dd>
            </dl>
        </div>
        <div class="btn-bar">
            <button class="btn"
                    data-url="<?php echo Url::to('Manager/add'); ?>"
                    data-jump="back"
                    type="button"
                    onclick="ajaxSubmit(this);"> 提交
            </button>
            <a class="btn btn-back" href="javascript:history.go(-1);">返回列表</a>
            <p class="error" id="form_notice"></p>
        </div>
    </form>
</main>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>
endsection
