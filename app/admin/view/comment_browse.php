@extends('base/layout_1')

@section('contents')
<main>
    <aside class="cat-tree">
        <div class="tt">
            <b>评论栏目</b>
        </div>
        <div class="cc">
            <p>
                <a href="<?php echo Url::to('Comment/browse'); ?>">全部</a>
            </p>
            <ul>
                <li>
                    <p><a href="<?php echo Url::to('Comment/browse', array('status' => 0)); ?>">未审阅</a></p>
                    <p><a href="<?php echo Url::to('Comment/browse', array('status' => 1)); ?>">已通过</a></p>
                    <p><a href="<?php echo Url::to('Comment/browse', array('is_admin' => 1)); ?>">管理员</a></p>
                </li>
            </ul>
        </div>
    </aside>
    <section>
        <header>
            <div>
                <a class="btn" data-url="<?php echo Url::to('Comment/del'); ?>" data-id="0" href="javascript:;"
                   onclick="goDel(this);">删除</a>
                <a class="btn" data-id="0" href="javascript:;" onclick="commentPass(this);">通过</a>
            </div>
        </header>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        <input id="check_all" type="checkbox">
                        comment_id
                    </th>
                    <th>obj_id</th>
                    <th>内容</th>
                    <th>昵称</th>
                    <th>邮箱</th>
                    <th>网址</th>
                    <th>##</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $fo): ?>
                    <tr>
                        <td>
                            <input data-checked="true" type="checkbox" value="<?php echo $fo['comment_id']; ?>">
                            <?php echo $fo['comment_id']; ?>
                        </td>
                        <td>
                            <?php echo $fo['obj_id']; ?>
                        </td>
                        <td>
                            <div style="color: #666;">
                                <?php echo $fo['quote']; ?>
                            </div>
                            <div>
                                <a href="javascript:;" onclick="openClose(this);">
                                    <small>[re]</small>
                                </a>
                                <?php echo $fo['content']; ?>
                            </div>
                            <div class="reply">
                                <form>
                                    <input type="hidden" name="to_comment_id" value="<?php echo $fo['comment_id']; ?>">
                                    <input type="hidden" name="channel" value="<?php echo $fo['channel']; ?>">
                                    <input type="hidden" name="obj_id" value="<?php echo $fo['obj_id']; ?>">
                                    <textarea name="content" class="form"></textarea>
                                    <a class="btn" style="vertical-align:bottom;" href="javascript:;"
                                       onclick="commentSubmit(this);">保存</a>
                                    <a class="close" href="javascript:;" onclick="replyClose(this);">&#xD7;</a>
                                </form>
                            </div>
                        </td>
                        <td>
                            <?php echo $fo['nickname']; ?>
                        </td>
                        <td>
                            <?php echo $fo['email']; ?>
                        </td>
                        <td>
                            <?php echo $fo['link']; ?>
                        </td>
                        <td>
                            <label id="comment_pass_btn_<?php echo $fo['comment_id']; ?>">
                                <?php if ($fo['status'] == 0): ?>
                                    [<span style="color: #ccc;">未审阅 |-></span>
                                    <a class="list-a"
                                       data-id="<?php echo $fo['comment_id']; ?>"
                                       href="javascript:;"
                                       onclick="commentPass(this);">通过</a>]
                                <?php else: ?>
                                    已通过
                                <?php endif; ?>
                            </label>

                            <a class="list-a" data-url="<?php echo Url::to('Comment/del'); ?>"
                               data-id="<?php echo $fo['comment_id']; ?>" href="javascript:;"
                               onclick="goDel(this);">删除</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <footer class="page">
            <?php echo $page_show; ?>
        </footer>
    </section>
</main>
endsection

@section('head')
<style>
    main > section {
        margin-left: 150px;
    }

    .cat-tree {
        width: 130px;
    }

    main td .reply {
        position: relative;
        float: left;
        display: none;
    }

    main td .reply .close:hover {
        color: #f00;
    }

    main td .reply .close {
        position: absolute;
        top: -4px;
        right: 0;

        color: #333;
        font-size: 20px;
    }
</style>
endsection

@section('foot')
<script>

    $(function () {

        /* 全选与取消 */
        $('#check_all').on('change', function () {
            $('input[data-checked]').prop("checked", this.checked);
        });
    });

    /* 打开回复区 */
    function openClose(that) {
        $(that).closest('td').find('.reply').slideDown();
    }

    /* 关闭回复区 */
    function replyClose(that) {
        $(that).closest('td').find('.reply').slideUp();
    }

    /* 提交评论 */
    function commentSubmit(that) {
        // 数据准备
        var form = $(that).closest('form');
        var url = '<?php echo Url::to('Comment/add'); ?>';
        var data = form.serialize();

        var code = -1;
        $.post(url, data, function (res) {
            console.log(res);
            if (res['code'] !== 0) {
                alert('评论出错：' + res.msg);
            } else {
                replyClose(that);
            }
        }, 'json');
    }

    /* 批量通过 */
    function commentPass(that) {
        var id = $(that).data('id');
        var ids = [];

        if (id > 0) {
            ids.push(id);
        } else {
            $('input[data-checked]').each(function (index, element) {
                if (true === $(element).prop('checked')) {
                    var id = parseInt($(element).val());
                    ids.push(id);
                }
            });
        }

        if (ids.length > 0) {
            var url = '<?php echo Url::to('Comment/check'); ?>';
            var data = {status: 1, ids: ids.toString()};
            $.post(url, data, function (res) {
                if (res.code === 0) {
                    // 修改按钮状态
                    for (var k in ids) {
                        $('#comment_pass_btn_' + ids[k]).text('已通过');
                    }
                } else {
                    alert('修改出错：' + res.msg);
                }
            }, 'json');
        } else {
            alert('没有任何选择');
        }
    }
</script>
endsection
