@extends('base/layout_1')

@section('contents')
<main>
    <aside class="cat-tree">
        <div class="tt">
            <b>xxx</b>
            <a class="float-right"
               href="<?php echo Url::to('Chapter/add', ['doc_id' => $doc_info['doc_id']]); ?>">[新增 +]</a>
        </div>
        <div class="cc">
            <?php echo $chapter_tree; ?>
        </div>
    </aside>

    <section>
        <form id="ajax_form" role="form" onsubmit="return false">
            <div class="input-panel">
                <dl>
                    <dt>
                        上级目录
                    </dt>
                    <dd>
                        <select class="form" name="parent_id">
                            <option value="0">--顶端，没有上级--</option>
                            <?php
                            foreach ($select_tree as $fo) {
                                echo '<option value="' . $fo['chapter_id'] . '"';
                                if ($parent_id == $fo['chapter_id']) {
                                    echo ' selected="selected"';
                                }
                                echo '>' . trim($fo['title'], '/') . '</option>';
                            }
                            ?>
                        </select>
                    </dd>
                    <dt>标题</dt>
                    <dd>
                        <input class="form form-large form-title" name="title" data-required_notice="请输入标题。" type="text"
                               value="">
                        <p class="error"></p>
                    </dd>
                    <dt>类型</dt>
                    <dd>
                        <label style="margin-right: 20px;">
                            <input type="radio" name="types" value="content" checked="checked"> 内容
                        </label>
                        <label>
                            <input type="radio" name="types" value="dir"> 目录
                        </label>
                    </dd>
                </dl>

                <!--编辑器主体-->
                <?php require Conf::$all['view_path'] . 'base/edit_body.php'; ?>
            </div>

            <div class="btn-bar clear">
                <button class="btn"
                        data-url="<?php echo Url::to('Chapter/add', ['doc_id' => $doc_info['doc_id']]); ?>"
                        data-jump="refresh"
                        type="button"
                        onclick="ajaxSubmit(this);"> 提交
                </button>
                <a class="btn btn-back" href="<?php echo Url::to('Doc/browse'); ?>">返回文档集</a>
                <p id="form_notice"></p>
            </div>
        </form>
    </section>
</main>
endsection

@section('head')
<!--编辑器样式-->
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/eewakeditor/eewakeditor.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/github-markdown.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/prism/prism.css">

<style>
    main > section {
        margin-left: 350px;
    }

    .cat-tree {
        width: 330px;
    }

    .form-title,
    .eewakeditor {
        width: 800px;
    }

    .eewakeditor textarea {
        height: 230px;
    }

    /* 拖动排序 */

    #sortable {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    #sortable li.sortable-target {
        border-bottom: 1px dashed #f00;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>

<script>

    $(function () {

        // 类型切换
        $('input[name=types]').on('change', function () {
            if ($(this).val() === 'content') {
                $('#eewakeditor').show();
                $('#eewakeditor').find('textarea').attr('name', 'content');
            } else {
                $('#eewakeditor').hide();
                $('#eewakeditor').find('textarea').removeAttr('name');
            }
        });
    });

</script>

<?php require Conf::$all['view_path'] . 'chapter/script.php'; ?>

<!--编辑器脚本-->
<?php require Conf::$all['view_path'] . 'base/edit_script.php'; ?>
endsection
