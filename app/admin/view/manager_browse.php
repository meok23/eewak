@extends('base/layout_1')

@section('contents')
<main>
    <section>
        <header>
            <div>
                <a class="btn" href="<?php echo Url::to('Manager/add'); ?>">新增</a>
                <a class="btn" data-url="<?php echo Url::to(); ?>?c=Manager&m=del" data-id="0" href="javascript:;"
                   onclick="goDel(this);">删除</a>
            </div>
        </header>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        <input id="check_all" type="checkbox">
                        用户名
                    </th>
                    <th>上次登录时间</th>
                    <th>上次登录IP</th>
                    <th>email</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $fo): ?>
                    <tr class="<?php if (1 !== (int)$fo['status']) echo 'forbid'; ?>">
                        <td>
                            <input data-checked="true" type="checkbox" value="<?php echo $fo['manager_id']; ?>">
                            <?php echo $fo['name']; ?>
                        </td>
                        <td><?php echo date('Y-m-d H:i', $fo['last_time']); ?></td>
                        <td><?php echo long2ip($fo['last_ip']); ?></td>
                        <td><?php echo $fo['email']; ?></td>
                        <td>
                            <?php
                            if (1 === (int)$fo['status']) {
                                echo '正常';
                            } else {
                                echo '已禁用';
                            }
                            ?>
                        </td>
                        <td>
                            <a class="list-a"
                               href="<?php echo Url::to('Manager/update', ['manager_id' => $fo['manager_id']]); ?>">修改</a>
                            <a class="list-a" data-url="<?php echo Url::to(); ?>?c=Manager&m=del"
                               data-id="<?php echo $fo['manager_id']; ?>" href="javascript:;"
                               onclick="goDel(this);">删除</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <footer class="page">
            <?php echo $page_show; ?>
        </footer>
    </section>
</main>
endsection

@section('head')
<style>
    .table tbody tr.forbid > td {
        color: #999;
    }
</style>
endsection

@section('foot')
<script>

    $(function () {

        /* 全选与取消 */
        $('#check_all').on('change', function () {
            $('input[data-checked]').prop("checked", this.checked);
        });
    });

</script>
endsection
