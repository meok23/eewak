@extends('base/layout_1')

@section('contents')
<main>
    <section>
        <header>
            <div>
                <a class="btn" href="<?php echo Url::to('Art/add'); ?>">新增</a>
                <a class="btn" href="javascript:;" data-url="<?php echo Url::to('Art/del'); ?>" data-id="0"
                   onclick="goDel(this);">删除</a>
            </div>
        </header>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        <input type="checkbox" id="check_all">
                        art_id
                    </th>
                    <th>标题</th>
                    <th>##</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $fo): ?>
                    <tr>
                        <td>
                            <label>
                                <input type="checkbox" data-checked="true" value="<?php echo $fo['art_id']; ?>">
                                <?php echo $fo['art_id']; ?>
                            </label>
                        </td>
                        <td>
                            <a href="<?php echo Url::to('Art/show', ['id' => $fo['art_id']], Conf::$all['front_script']); ?>"
                               target="_blank">
                                <small>[link]</small>
                            </a>
                            <span <?php if ($fo['is_hide'] == 1) echo 'style="color:#ccc"'; ?>>
                            <?php echo $fo['title']; ?>
                            </span>
                        </td>
                        <td>
                            <a class="list-a"
                               href="<?php echo Url::to('Art/update', ['art_id' => $fo['art_id']]); ?>">修改</a>
                            <a class="list-a" href="javascript:;"
                               data-url="<?php echo Url::to('Art/del'); ?>"
                               data-id="<?php echo $fo['art_id']; ?>" onclick="goDel(this);">删除</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <footer class="page">
            <?php echo $page_show; ?>
        </footer>
    </section>
</main>
endsection

@section('foot')
<script>

    $(function () {

        /* 全选与取消 */
        $('#check_all').on('change', function () {
            $('input[data-checked]').prop("checked", this.checked);
        });
    });

</script>
endsection
