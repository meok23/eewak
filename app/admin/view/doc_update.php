@extends('base/layout_1')

@section('contents')
<main>
    <form id="ajax_form" role="form" onsubmit="return false">
        <input class="form" name="doc_id" type="hidden" value="<?php echo $data['doc_id']; ?>">
        <div class="input-panel">
            <dl>
                <dt>
                    危险操作
                    <small>(删除文档会同时删除其下的所有章节)</small>
                </dt>
                <dd>
                    <input class="form form-del" id="delInput" type="text" placeholder="请输入文档标题">
                    <a class="btn btn-del" href="javascript:;" onclick="del_doc();">删除</a>
                    <hr>
                </dd>
                <dt>标题</dt>
                <dd>
                    <input class="form" name="title" data-required_notice="请输入标题。" type="text"
                           value="<?php echo $data['title']; ?>">
                    <p class="error"></p>
                </dd>
                <dt>简介</dt>
                <dd>
                    <textarea class="form" name="brief"><?php echo $data['brief']; ?></textarea>
                </dd>
            </dl>

            <p>
                <span>类型: </span>
                <?php if ($data['types'] === 'url') {
                    echo '外链[url]';
                } else {
                    echo '文档[doc]';
                } ?>
            </p>
            <?php if ($data['types'] === 'url'): ?>
                <div>
                    <input class="form" name="url" type="text" value="<?php echo $data['url']; ?>">
                </div>
            <?php endif; ?>
        </div>

        <div class="btn-bar">
            <button class="btn"
                    data-url="<?php echo Url::to('Doc/update'); ?>"
                    data-jump="back"
                    type="button"
                    onclick="ajaxSubmit(this);"> 提交
            </button>
            <a class="btn btn-back" href="javascript:history.go(-1);">返回列表</a>
            <p id="form_notice"></p>
        </div>
    </form>
</main>
endsection

@section('head')
<style>
    .input-panel {
        padding-bottom: 30px;
    }

    .form-del {
        width: 276px;
    }

    .btn-del, .btn-del:hover {
        background-color: #ff0000;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>

<script>

    function del_doc() {
        var url = '<?php echo Url::to('Doc/del'); ?>';
        var doc_id = <?php echo $data['doc_id']; ?>;
        var signs = $('#delInput').val();

        $.post(url, {"doc_id": doc_id, "signs": signs}, function (res) {

            if (0 === res.code) {
                $('#form_notice').html('删除成功。');

                history.go(-1);
            } else if (403 === res.code) {
                $('#form_notice').html('没有权限。');
            } else {
                $('#form_notice').html('操作失败。' + res.msg);
            }
        }, 'json');
    }

</script>
endsection
