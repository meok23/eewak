<?php

use fw\tool\Url;

?>

<script>
    function checkAlias() {
        var that = arguments[0];

        var url = '<?php echo Url::to('Page/checkAlias'); ?>';
        $.get(url, {alias: $(that).val()}, function (res) {
            if (res.code !== 0) {
                $(that).nextAll('.error').text(res.msg);
            }
        }, 'json');
    }
</script>
