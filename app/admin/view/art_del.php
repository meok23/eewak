@extends('base/layout_1')

@section('contents')
<main>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th>
                    <input id="check_all" type="checkbox">
                    art_id
                </th>
                <th>标题</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $fo): ?>
                <tr>
                    <td>
                        <input type="checkbox" data-checked="true" value="<?php echo $fo['art_id']; ?>"
                               checked="checked">
                        <?php echo $fo['art_id']; ?>
                    </td>
                    <td><?php echo $fo['title']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="notice">
            暂存到回收站！
        </p>
    </div>
    <div class="Confirm-del" id="ajax_form">
        <a href="javascript:;" class="btn" data-url="<?php echo Url::to('Art/del'); ?>"
           onclick="ajaxDel(this);">确定删除</a>
        <a href="javascript:history.go(-1);" class="btn btn-back">返回列表</a>
        <p id="form_notice" class="error"></p>
    </div>
</main>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>
endsection
