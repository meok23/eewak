@extends('base/layout_1')

@section('contents')
<main>
    <form id="ajax_form" role="form" onsubmit="return false">
        <input class="form" name="page_id" type="hidden" value="<?php echo $data['page_id']; ?>">

        <div class="input-panel">
            <dl>
                <dt>标题</dt>
                <dd>
                    <input class="form form-title" name="title" data-required_notice="请输入标题。" type="text"
                           value="<?php echo $data['title']; ?>">
                    <p class="error"></p>
                </dd>
                <dt>
                    别名
                    <small>(别名唯一，作URL标识)</small>
                </dt>
                <dd>
                    <input class="form" name="alias" data-required_notice="请输入别名。" type="text"
                           value="<?php echo $data['alias']; ?>"
                           onchange="checkAlias(this);">
                    <span class="error"></span>
                </dd>
                <dt>内容</dt>
                <dd class="form-content">
                    <!--编辑器主体-->
                    <?php require Conf::$all['view_path'] . 'base/edit_body.php'; ?>
                </dd>
            </dl>
        </div>

        <div class="btn-bar clear">
            <button class="btn"
                    data-url="<?php echo Url::to('Page/update'); ?>"
                    data-jump="back"
                    type="button"
                    onclick="ajaxSubmit(this);"> 提交
            </button>
            <a class="btn btn-back" href="javascript:history.go(-1);">返回列表</a>
            <p id="form_notice"></p>
        </div>
    </form>
</main>
endsection

@section('head')
<!--编辑器样式-->
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/eewakeditor/eewakeditor.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/github-markdown.css">
<link rel="stylesheet" href="<?php echo Url::root(); ?>assets/lib/prism/prism.css">

<style>
    .form-title,
    .eewakeditor {
        width: 800px;
    }

    .eewakeditor textarea {
        height: 230px;
    }

    .form-cat > p {
        margin: 4px;
    }
</style>
endsection

@section('foot')
<script src="<?php echo Url::root(); ?>assets/lib/_validate.js"></script>

<?php require Conf::$all['view_path'] . 'page/script.php'; ?>

<!--编辑器脚本-->
<?php require Conf::$all['view_path'] . 'base/edit_script.php'; ?>
endsection
