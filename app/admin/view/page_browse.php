@extends('base/layout_1')

@section('contents')
<main>
    <section>
        <header>
            <div>
                <a class="btn" href="<?php echo Url::to('Page/add'); ?>">新增</a>
                <a class="btn" href="javascript:;" data-url="<?php echo Url::to('Page/del'); ?>"
                   data-id="0"
                   onclick="goDel(this);">删除</a>
            </div>
        </header>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        <input type="checkbox" id="check_all">
                        page_id
                    </th>
                    <th>alias</th>
                    <th>标题</th>
                    <th>##</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $fo): ?>
                    <tr>
                        <td>
                            <label>
                                <input type="checkbox" data-checked="true" value="<?php echo $fo['page_id']; ?>">
                                <?php echo $fo['page_id']; ?>
                            </label>
                        </td>
                        <td>
                            <?php echo $fo['alias']; ?>
                        </td>
                        <td>
                            <a href="<?php echo Url::to('Page/show', ['alias' => $fo['alias']], Conf::$all['front_script']); ?>"
                               target="_blank">
                                <small>[link]</small>
                            </a>
                            <?php echo $fo['title']; ?>
                        </td>
                        <td>
                            <a class="list-a"
                               href="<?php echo Url::to('Page/update', ['page_id' => $fo['page_id']]); ?>">修改</a>
                            <a class="list-a" href="javascript:;"
                               data-url="<?php echo Url::to('Page/del'); ?>"
                               data-id="<?php echo $fo['page_id']; ?>" onclick="goDel(this);">删除</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <footer class="page">
            <?php echo $page_show; ?>
        </footer>
    </section>
</main>
endsection

@section('foot')
<script>

    $(function () {

        /* 全选与取消 */
        $('#check_all').on('change', function () {
            $('input[data-checked]').prop("checked", this.checked);
        });
    });

</script>
endsection
