<?php

/**
 * 评论控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace app\admin\controller;

use app\admin\model\Comment;
use app\common\logic\Response;
use fw\base\Conf;
use fw\base\View;
use fw\tool\Json;
use fw\tool\Request;
use fw\tool\Session;
use fw\tool\Str;

class ControllerComment extends Base
{
    public function add()
    {
        // 参数补充
        $_POST['nickname'] = '系统管理员';
        $_POST['user_id'] = Session::gi()->get('auth.manager_id');
        $_POST['is_admin'] = 1;

        $ret = Comment::gi()->add();
        echo Json::encode($ret);
    }

    public function check()
    {
        $_POST['comment_ids'] = $_POST['ids'];
        $ret = Comment::gi()->check();

        echo Json::encode($ret);
    }

    public function browse()
    {
        // 参数获取
        $status = Request::get('status');
        $is_admin = Request::get('is_admin');

        $length = Conf::$all['page_length'];
        $current_page = Request::get('page', null, 1);

        // 条数总计
        $list_count = Comment::gi()->getList([
            'status'   => $status,
            'is_admin' => $is_admin,
            'is_count' => true,
        ]);
        $page_total = ceil($list_count / $length);

        // 分页
        $option = [
            'current_page' => $current_page,
            'page_total'   => $page_total,
            'list_count'   => $list_count,
            'param'        => ['status' => $status, 'is_admin' => $is_admin],
        ];
        $page = new \app\common\logic\Page($option);
        $page_show = $page->show();

        // 查询列表
        $data = Comment::gi()->getList([
            'status'   => $status,
            'is_admin' => $is_admin,
            'length'   => $length,
            'page'     => $current_page,
        ]);

        // 输出到视图
        $assign = [
            'data'      => $data,
            'page_show' => $page_show,
        ];
        View::render('comment_browse', $assign);
    }

    public function del()
    {
        if (Request::isPost()) {
            // 验证密码是否正确
            $is_carry = false;
            $password = Request::post('manager_password', 'trim');
            $email = Session::gi()->get('auth.email');

            if (!empty($password)) {
                $password = Str::encrypt($password, $password);
                $identities = Str::encrypt($email, $email);

                // 查询数据库
                $where = "identities='{$identities}' AND password='{$password}'";
                $ret = \fw\db\DB::table('manager')->where($where)->count();
                ($ret > 0) && $is_carry = true;
            }
            if ($is_carry == false) {
                $resp = Response::to([20, 'Password error.']);
                echo Json::encode($resp);
                return;
            }

            // 密码验证通过才允许下一步操作
            if ($is_carry == true) {
                $comment_ids_raw = Request::post('ids', 'trim');
                $comment_ids = explode(',', $comment_ids_raw);
                $op_user = Session::gi()->get('auth.manager_id');

                $resp = Response::to();
                foreach ($comment_ids as $comment_id) {
                    $resp = Comment::gi()->del(['comment_id' => $comment_id, 'op_user' => $op_user]);
                    if ($resp['code'] != 0) {
                        break;
                    }
                }
                echo Json::encode($resp);
            }
        } else {
            $comment_ids = Request::get('ids', 'trim');

            if (empty($comment_ids)) {
                View::error(['msg' => 'Require comment_id.']);
            }

            $data = Comment::gi()->getList(['comment_ids' => $comment_ids]);

            View::render('comment_del', ['data' => $data]);
        }
    }
}
