<?php

/**
 * 单页控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace app\admin\controller;

use app\admin\model\Page;
use app\common\logic\Response;
use fw\base\Conf;
use fw\base\View;
use fw\tool\Json;
use fw\tool\Request;
use fw\tool\Session;

class ControllerPage extends Base
{
    public function add()
    {
        if (Request::isPost()) {
            $_POST['manager_id'] = Session::gi()->get('auth.manager_id');

            if (empty($_POST['alias'])) {
                View::error(['msg' => 'Require alias.']);
            }

            $ret = Page::gi()->add();

            echo Json::encode($ret);
        } else {
            View::render('page_add');
        }
    }

    public function update()
    {
        if (Request::isPost()) {
            $_POST['manager_id'] = Session::gi()->get('auth.manager_id');

            if (empty($_POST['alias'])) {
                View::error(['msg' => 'Require alias.']);
            }

            $ret = Page::gi()->update();
            $ret['code'] == 200 && $ret['code'] = 0;

            echo Json::encode($ret);
        } else {
            $page_id = Request::get('page_id');

            $data = Page::gi()->getInfo(['page_id' => $page_id]);

            View::render('page_update', ['data' => $data]);
        }
    }

    public function browse()
    {
        $length = Conf::$all['page_length'];
        $current_page = (int)Request::get('page', null, 1);

        // 条数总计
        $list_count = Page::gi()->getList(['types' => 'page', 'is_count' => true]);
        $page_total = ceil($list_count / $length);

        // 分页
        $option = [
            'current_page' => $current_page,
            'page_total'   => $page_total,
            'list_count'   => $list_count,
        ];
        $page = new \app\common\logic\Page($option);
        $page_show = $page->show();

        // 查询列表
        $data = Page::gi()->getList([
            'length' => $length,
            'page'   => $current_page,
        ]);

        // 输出到视图
        View::render('page_browse', ['data' => $data, 'page_show' => $page_show]);
    }

    public function checkAlias()
    {
        $alias = Request::get('alias', 'trim');
        $info = Page::gi()->getInfo(['alias' => $alias]);

        if (!empty($info)) {
            $code = -1;
            $msg = '别名不可用，改一个吧';
        } else {
            $code = 0;
            $msg = '';
        }

        $resp = Response::to([$code, $msg]);
        echo Json::encode($resp);
    }

    public function del()
    {
        if (Request::isPost()) {
            $page_ids_raw = Request::post('ids', 'trim');
            $page_ids = explode(',', $page_ids_raw);
            $op_user = Session::gi()->get('auth.manager_id');

            $resp = Response::to();
            foreach ($page_ids as $page_id) {
                $resp = Page::gi()->del(['page_id' => $page_id, 'op_user' => $op_user]);
                if ($resp['code'] != 0) break;
            }

            echo Json::encode($resp);
        } else {
            $page_ids = Request::get('ids', 'trim');
            if (empty($page_ids)) {
                View::error(['msg' => 'Require msg_id.']);
            }

            $data = Page::gi()->getList(['page_ids' => $page_ids]);

            View::render('page_del', ['data' => $data]);
        }
    }
}
