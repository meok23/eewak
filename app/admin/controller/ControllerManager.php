<?php

/**
 * 管理员操作控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace app\admin\controller;

use app\admin\model\Manager;
use app\common\logic\Response;
use fw\base\Conf;
use fw\base\View;
use fw\db\DB;
use fw\tool\Json;
use fw\tool\Request;
use fw\tool\Session;
use fw\tool\Str;

class ControllerManager extends Base
{
    public function add()
    {
        if (Request::isPost()) {
            // 剔除全是 "0" 的密码
            if (preg_match("/^0+$/", trim($_POST['password'])) > 0) {
                unset($_POST['password']);
            }

            $_POST['parent_id'] = Session::gi()->get('auth.manager_id');
            $ret = Manager::gi()->add();

            echo Json::encode($ret);
        } else {
            View::render('manager_add');
        }
    }

    public function update()
    {
        if (Request::isPost()) {
            // 剔除全是 "0" 的密码
            if (preg_match("/^0+$/", trim($_POST['password'])) > 0) {
                unset($_POST['password']);
                unset($_POST['re_password']);
            }

            $ret = Manager::gi()->update();
            $ret['code'] == 200 && $ret['code'] = 0;

            echo Json::encode($ret);
        } else {
            $manager_id = (int)Request::get('manager_id');
            if ($manager_id < 1) {
                View::error(['msg' => 'Require manager_id.']);
            }

            $data = Manager::gi()->getInfo(['manager_id' => $manager_id]);

            View::render('manager_update', ['data' => $data]);
        }
    }

    /**
     * 个人中心
     */
    public function myzone()
    {
        if (Request::isPost()) {
            // 剔除全是 "0" 的密码
            if (preg_match("/^0+$/", trim($_POST['password'])) > 0) {
                unset($_POST['password']);
                unset($_POST['re_password']);
            }

            $ret = Manager::gi()->update();
            $ret['code'] == 200 && $ret['code'] = 0;

            echo Json::encode($ret);

        } else {
            $manager_id = Session::gi()->get('auth.manager_id');
            if ($manager_id < 1) {
                View::error(['msg' => 'Require manager_id.']);
            }

            $data = Manager::gi()->getInfo(['manager_id' => $manager_id]);

            View::render('manager_update', ['data' => $data]);
        }
    }

    public function browse()
    {
        // 参数获取
        $length = Conf::$all['page_length'];
        $current_page = (int)Request::get('page', null, 1);

        $list_count = Manager::gi()->getList(['is_count' => true]);
        $page_total = ceil($list_count / $length);

        // 分页
        $option = [
            'current_page' => $current_page,
            'page_total'   => $page_total,
            'list_count'   => $list_count,
        ];
        $page = new \app\common\logic\Page($option);
        $page_show = $page->show();

        // 查询列表
        $data = Manager::gi()->getList(['length' => $length, 'page' => $current_page]);

        View::render('manager_browse', ['page_show' => $page_show, 'data' => $data]);
    }

    public function del()
    {
        if (Request::isPost()) {
            // 验证密码是否正确
            $password = Request::post('manager_password', 'trim');
            $email = Session::gi()->get('auth.email');
            if (!empty($password)) {
                $password = Str::encrypt($password, $password);
                $identities = Str::encrypt($email, $email);

                // 查询数据库
                $where = "identities='{$identities}' AND password='{$password}'";
                $ret = DB::table('manager')->where($where)->count();
                ($ret < 1) && $password = null;
            }
            if (empty($password)) {
                $resp = Response::to([20, 'Password error.']);

                echo Json::encode($resp);
                return;
            }

            // 密码验证通过才允许下一步操作
            $manager_ids_raw = Request::post('ids', 'trim');
            $manager_ids = explode(',', $manager_ids_raw);
            $op_user = Session::gi()->get('auth.manager_id');

            $resp = Response::to();
            foreach ($manager_ids as $manager_id) {
                $resp = Manager::gi()->del(['manager_id' => $manager_id, 'op_user' => $op_user]);
                if ($resp['code'] != 0) {
                    break;
                }
            }
            echo Json::encode($resp);
        } else {
            $manager_ids = Request::get('ids', 'trim');
            if (empty($manager_ids)) {
                View::error(['msg' => 'Require manager_id.']);
            }

            $data = Manager::gi()->getList(['manager_ids' => $manager_ids]);

            View::render('manager_del', ['data' => $data]);
        }
    }
}
