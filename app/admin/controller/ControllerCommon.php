<?php

/**
 * 公用控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace app\admin\controller;

use app\admin\model\Art;
use app\admin\model\Login;
use app\common\logic\Response;
use ext\Image;
use fw\base\View;
use fw\tool\Json;
use fw\tool\Request;
use fw\tool\Url;

class ControllerCommon
{
    public function login()
    {
        View::render('login');
    }

    public function doLogin()
    {
        $ret = Login::gi()->login();

        // 成功跳转
        if ($ret['code'] === 0) {
            $url = Url::to();
            Header("Location: {$url}");
        } else {
            View::error($ret);
        }
    }

    public function logout()
    {
        Login::gi()->logout();

        $url = Url::to('Common/login');
        Header("Location: {$url}");
    }

    public function uploadImg()
    {
        $upload_file = current($_FILES);

        $image = new Image();
        $image->open($upload_file['tmp_name']);
        $type = $image->getType();
        $image->thumb(1024, 0, 1);

        $path = WEB_PATH . 'upload/img/';
        !is_dir($path) && mkdir($path, 0777, true);

        $filename = date('YmdHi') . '_' . uniqid() . '.' . $type['type'];
        $image->save($path . $filename);
        $src = Url::root() . 'upload/img/' . $filename;

        $resp = Response::to([0, 'Success.', ['src' => $src, 'title' => $upload_file['name']]]);
        echo Json::encode($resp);
    }
}
