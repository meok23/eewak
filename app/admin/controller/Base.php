<?php

/**
 * 基础控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-02
 */

namespace app\admin\controller;

use app\admin\model\Login;
use fw\base\Conf;
use fw\tool\Session;
use fw\tool\Url;

class Base
{
    public function __construct()
    {
        // 初始化
        $this->__init();

        // 登录验证
        $this->_checkLogin();

        // 输出一些初始化信息，比如：用户名
        Conf::$all['render']['base_username'] = Session::gi()->get('auth.name');
    }

    protected function __init()
    {
        // 这个空方法必须保留，给子类继承
    }

    /**
     * 登录验证
     */
    private function _checkLogin()
    {
        $check_auth = Session::gi()->has('auth');

        if ($check_auth == false && !Login::gi()->autoLogin()) {

            // 尚未登陆，跳转到登录页
            $url = Url::to('Common/login');
            Header("Location: {$url}");
        }
    }
}
