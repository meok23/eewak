<?php

/**
 * 文档章节控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace app\admin\controller;

use app\admin\model\Chapter;
use app\admin\model\Doc;
use app\common\logic\Response;
use fw\base\View;
use fw\db\DB;
use fw\tool\Arr;
use fw\tool\Json;
use fw\tool\Request;
use fw\tool\Session;

class ControllerChapter extends Base
{
    public function add()
    {
        if (Request::isPost()) {
            // 参数获取
            $_POST['manager_id'] = Session::gi()->get('auth.manager_id');
            $_POST['doc_id'] = (int)Request::get('doc_id');
            $ret = Chapter::gi()->add();

            echo Json::encode($ret);
        } else {
            // 参数获取
            $doc_id = (int)Request::get('doc_id');

            // 参数校验
            if ($doc_id < 1) {
                View::error(['msg' => 'Require doc_id.']);
            }

            $chapter_rows = Chapter::gi()->getList(['doc_id' => $doc_id]);

            // 文档信息
            $doc_info = Doc::gi()->getInfo(['doc_id' => $doc_id]);

            // 章节树
            $chapter_tree = Chapter::gi()->ChapterTree($chapter_rows);

            // 查询备选的上级目录
            $chapter_rows_dir = [];
            foreach ($chapter_rows as $fo) {
                if ($fo['types'] === 'dir') {
                    $chapter_rows_dir[] = $fo;
                }
            }
            $parent_id = (isset($_GET['parent_id']) && $_GET['parent_id'] > 0) ? (int)$_GET['parent_id'] : 0;
            $select_tree = Chapter::gi()->TreeWithTitle($chapter_rows_dir);

            View::render('chapter_add', [
                'doc_info'     => $doc_info,
                'chapter_tree' => $chapter_tree,
                'parent_id'    => $parent_id,
                'select_tree'  => $select_tree,
            ]);
        }
    }

    public function update()
    {
        if (Request::isPost()) {
            // 记录 manager_id
            $_POST['manager_id'] = Session::gi()->get('auth.manager_id');

            // 更新数据
            $ret = Chapter::gi()->update();
            $ret['code'] == 200 && $ret['code'] = 0;

            echo Json::encode($ret);
        } else {
            // 参数获取
            $doc_id = (int)Request::get('doc_id');
            $chapter_id = (int)Request::get('chapter_id');

            // 参数校验
            if ($doc_id < 1) {
                View::error(['msg' => 'Require doc_id.']);
            }

            $chapter_rows = Chapter::gi()->getList(['doc_id' => $doc_id]);

            // 文档信息
            $doc_info = Doc::gi()->getInfo(['doc_id' => $doc_id]);

            // 章节树
            $chapter_tree = Chapter::gi()->ChapterTree($chapter_rows);

            // 查询数据
            $data = Chapter::gi()->getInfo(['doc_id' => $doc_id, 'chapter_id' => $chapter_id]);

            // 查询备选的上级目录
            $chapter_rows_dir = [];
            foreach ($chapter_rows as $fo) {
                if ($fo['types'] === 'dir') {
                    $chapter_rows_dir[] = $fo;
                }
            }
            $rs_all = Chapter::gi()->TreeWithTitle($chapter_rows_dir);
            $rs_sub = Chapter::gi()->TreeWithTitle($chapter_rows_dir, $chapter_id);
            $sub_chapter_ids = Arr::column($rs_sub, 'chapter_id');

            $select_tree = [];
            foreach ($rs_all as $k => $item) {
                if ($chapter_id == $item['chapter_id'] || in_array($item['chapter_id'], $sub_chapter_ids)) {
                    $item['chapter_id'] = -1;
                }

                $select_tree[] = $item;
            }

            View::render('chapter_update', [
                'data'         => $data,
                'doc_info'     => $doc_info,
                'chapter_tree' => $chapter_tree,
                'select_tree'  => $select_tree,
            ]);
        }
    }

    public function setSort()
    {
        $chapter_ids = Request::post('chapter_ids', 'trim');

        if (!empty($chapter_ids)) {
            $chapter_id_arr = explode(',', $chapter_ids);
            $chapter_id_arr = array_reverse($chapter_id_arr);

            $manager_id = Session::gi()->get('auth.manager_id');
            foreach ($chapter_id_arr as $sort => $chapter_id) {
                $ret = Chapter::gi()->update(['chapter_id' => $chapter_id, 'manager_id' => $manager_id, 'sort' => $sort]);
                if ($ret['code'] != 0) {
                    echo Json::encode($ret);
                    break;
                }
            }
        }
    }

    public function del()
    {
        $chapter_id = (int)Request::post('id');

        // 查询当前目录是否为空

        $info = DB::table('chapter')->where('parent_id=:parent_id', [':parent_id' => $chapter_id])->selectRow();
        if (!empty($info)) {
            $resp = Response::to([400, '删除失败，要删除的目录必须为空']);
            echo Json::encode($resp);
            return;
        }

        // 删除章节
        $ret = Chapter::gi()->del([
            'chapter_id' => $chapter_id,
            'op_user'    => Session::gi()->get('auth.manager_id'),
        ]);

        echo Json::encode($ret);
    }
}
