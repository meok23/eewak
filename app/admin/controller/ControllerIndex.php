<?php

/**
 * 首页控制器
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace app\admin\controller;

use fw\base\View;
use fw\tool\Url;

class ControllerIndex extends Base
{
    public function index()
    {
        // View::render('index_welcome');

        Header('Location: ' . Url::to('Art/browse'));
    }
}
