<?php

/**
 * 评论模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-16
 */

namespace app\common\model;

use app\common\logic\Response;
use fw\base\Conf;
use fw\db\DB;
use fw\di\DiUse;
use fw\tool\Request;

class Comment implements DiUse
{
    // 所有支持的频道
    private $_channels = ['art'];

    private $_di = null;

    public function setDi($di)
    {
        $this->_di = $di;
    }

    public function getDi()
    {
        return $this->_di['mail'];
    }

    /**
     * 新增一条数据
     *
     * @return array
     */
    public function add()
    {
        // 参数获取
        $channel = Request::post('channel', 'trim');
        $user_id = (int)Request::post('user_id');
        $obj_id = (int)Request::post('obj_id');
        $to_comment_id = (int)Request::post('to_comment_id');
        $status = (int)Request::post('status');
        $quote = Request::post('quote', 'trim');
        $content = Request::post('content', 'trim');
        $nickname = Request::post('nickname', 'trim');
        $email = Request::post('email', 'trim');
        $link = Request::post('link', 'trim');
        $manager_id = (int)Request::post('manager_id');
        $is_admin = (int)Request::post('is_admin');

        // 参数校验
        if (
            !in_array($channel, $this->_channels)
            || !in_array($status, [0, 1])
            || !in_array($is_admin, [0, 1])
            || $obj_id < 1
            || empty($content)
            || empty($nickname)
        ) {
            return Response::to([400, 'Parameter error.']);
        }

        // 填充引用
        if ($to_comment_id > 0) {
            $info = $this->getInfo(['comment_id' => $to_comment_id]);
            empty($info) && $to_comment_id = 0;
            $quote = '`' . $info['nickname'] . '`' . $info['content'];
        }

        // 数据准备
        $data = [
            'channel'     => $channel,
            'user_id'     => $user_id,
            'obj_id'      => $obj_id,
            'to_comment_id'   => $to_comment_id,
            'status'      => $status,
            'quote'       => $quote,
            'content'     => $content,
            'nickname'    => $nickname,
            'email'       => $email,
            'link'        => $link,
            'manager_id'  => $manager_id,
            'is_admin'    => $is_admin,
            'create_time' => time(),
            'update_time' => 0,
        ];

        // 操作数据库
        $ret = DB::table('comment')->insert($data);

        if (false == $ret) {
            return Response::log([500, 'Insert fail.', $data], Request::post());
        }
        $comment_id = DB::$dbh->lastInsertId();

        // 发邮件
        if ($to_comment_id > 0 && Conf::$all['is_comment_mail']) {
            if (!empty($info['email'])) {
                $subject = Conf::$all['comment_mail_subject'];
                $this->getDi()->sendMail($info['email'], $subject, $content);
            }
        }

        return Response::to([0, 'Success.', ['comment_id' => $comment_id]]);
    }

    /**
     * 获取一条数据
     *
     * @param array $args
     *
     * @return array
     */
    public function getInfo($args)
    {
        // 参数获取
        $comment_id = (int)Request::input($args, 'comment_id');

        $ret = DB::table('comment')->where('comment_id=:comment_id', [':comment_id' => $comment_id])->selectRow();

        return $ret;
    }

    /**
     * 获取列表或统计条数
     *
     * @param array $args
     *
     * @return array|int
     */
    public function getList($args)
    {
        // 参数获取
        $comment_ids = Request::input($args, 'comment_ids', 'trim');
        $channel = Request::input($args, 'channel', 'trim');
        $obj_id = (int)Request::input($args, 'obj_id');
        $status = (int)Request::input($args, 'status', null, -1);
        $is_admin = (int)Request::input($args, 'is_admin', null, -1);

        $page = Request::input($args, 'page', 'intval', 1);
        $length = Request::input($args, 'length', 'intval', 100);

        $sort_way = Request::input($args, 'sort_way', 'trim', 'DESC');
        $sort_way = strtoupper($sort_way);

        $is_count = Request::input($args, 'is_count', 'boolval', false);

        // 条件组装
        $prep = $map = [];
        if (!empty($comment_ids)) {
            $map[] = 'comment_id IN (' . $comment_ids . ')';
        }
        if (in_array($channel, $this->_channels)) {
            $map[] = "channel=:channel";
            $prep[':channel'] = $channel;

            if ($obj_id > 0) {
                $map[] = 'obj_id=:obj_id';
                $prep[':obj_id'] = $obj_id;
            }
        }
        if (in_array($status, [0, 1])) {
            $map[] = 'status=:status';
            $prep[':status'] = $status;
        }
        if (in_array($is_admin, [0, 1])) {
            $map[] = 'is_admin=:is_admin';
            $prep[':is_admin'] = $is_admin;
        }
        $where = implode(' AND ', $map);

        // 查询条数
        if ($is_count) {
            return DB::table('comment')->where($where, $prep)->count();
        }

        // 查询列表
        $rows = DB::table('comment')
            ->where($where, $prep)
            ->order('comment_id ' . $sort_way)
            ->limit(($page - 1) * $length . ',' . $length)
            ->select();

        return $rows;
    }
}
