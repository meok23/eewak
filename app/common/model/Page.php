<?php

/**
 * 单页模型
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-08-03
 */

namespace app\common\model;

use fw\db\DB;
use fw\tool\Request;

class Page
{
    protected $_tbl = 'page';

    /**
     * 获取一条数据
     *
     * @param array $args
     *
     * @return array
     */
    public function getInfo($args)
    {
        // 参数获取
        $page_id = (int)Request::input($args, 'page_id');
        $alias = Request::input($args, 'alias', 'trim');

        $page = [];
        if ($page_id > 0) {
            $page = DB::table($this->_tbl)
                ->where('page_id=:page_id', [':page_id' => $page_id])
                ->selectRow();
        } elseif (!empty($alias)) {
            $page = DB::table($this->_tbl)
                ->where('alias=:alias', [':alias' => $alias])
                ->selectRow();
        }

        return $page;
    }

    /**
     * 获取列表或统计条数
     *
     * @param array $args
     *
     * @return array|int
     */
    public function getList($args = [])
    {
        // 参数获取
        $page_ids = Request::input($args, 'page_ids', 'trim');

        $is_hide = Request::input($args, 'is_hide', function ($pa) {
            return in_array($pa, [0, 1]) ? $pa : 0;
        });

        $page = Request::input($args, 'page', 'intval', 1);
        $length = Request::input($args, 'length', 'intval', 100);

        $sort_way = Request::input($args, 'sort_way', 'trim', 'DESC');
        $sort_way = strtoupper($sort_way);

        $is_count = Request::input($args, 'is_count', 'boolval', false);

        // 条件组装
        $prep = $map = [];
        if (!empty($page_ids)) {
            $map[] = 'page_id IN (' . $page_ids . ')';
        }
        if (!is_null($is_hide)) {
            $map[] = 'is_hide=:is_hide';
            $prep[':is_hide'] = $is_hide;
        }
        $where = implode(' AND ', $map);

        // 查询条数
        if ($is_count === true) {
            return DB::table($this->_tbl)->where($where, $prep)->count();
        }

        // 查询列表
        $rows = DB::table($this->_tbl)
            ->where($where, $prep)
            ->order('page_id ' . $sort_way)
            ->limit(($page - 1) * $length . ',' . $length)
            ->select();

        return $rows;
    }
}
