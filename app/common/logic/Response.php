<?php

/**
 * 响应
 *
 * @author 煤老板 <meok23@sina.com>
 * @data   2017-06-05
 */

namespace app\common\logic;

class Response
{
    /**
     * 响应
     *
     * @param array $resp_raw
     *
     * @return array
     */
    public static function to($resp_raw = [])
    {
        $resp = [];
        $resp['code'] = (isset($resp_raw[0]) && is_int($resp_raw[0])) ? $resp_raw[0] : -10000;
        $resp['msg'] = isset($resp_raw[1]) ? trim($resp_raw[1]) : '';
        $resp['data'] = isset($resp_raw[2]) ? (array)$resp_raw[2] : [];

        return $resp;
    }

    /**
     * 响应并记录日志
     *
     * @param array $resp_raw
     * @param null  $param
     *
     * @return array
     */
    public static function log($resp_raw, $param = null)
    {
        $resp = self::to($resp_raw);

        $backtrace = debug_backtrace();

        // 注意 backtrace 的层次
        Log::gi()->add([
            'filename'   => $backtrace[0]['file'],
            'breakpoint' => $backtrace[1]['function'],
            'response'   => $resp,
            'param'      => $param,
        ]);

        return $resp;
    }
}
