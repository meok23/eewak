<?php

/**
 * 单例模式
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-02
 */

namespace fw\base;

trait Instance
{
    private static $_instance = null;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }

        return self::$_instance;
    }

    public static function gi()
    {
        return self::getInstance();
    }
}
