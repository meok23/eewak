<?php

/**
 * 路由类
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-01
 */

namespace fw\base;

use fw\tool\Str;

class Rout
{
    use Instance;

    public $currentController = '';
    public $currentMethod = '';

    public function __construct()
    {
        empty($_GET['c']) && $_GET['c'] = 'Index';
        empty($_GET['m']) && $_GET['m'] = 'index';

        $this->currentController = ucfirst(Str::lineToHump($_GET['c']));
        $this->currentMethod = Str::lineToHump($_GET['m']);
    }

    /**
     * 程序执行入口
     */
    public function entry()
    {
        $this->check();

        $this->exec($this->currentController, $this->currentMethod);
    }

    /**
     * 执行控制器方法
     *
     * @param $controller
     * @param $method
     */
    public function exec($controller, $method)
    {

        $filename = ROOT_PATH . 'app/' . MODULE_NAME . '/controller/Controller' . $controller . '.php';
        $controller = '\app\\' . MODULE_NAME . '\controller\Controller' . $controller;

        if (file_exists($filename) && ($con = new $controller()) && is_callable([$controller, $method])) {
            $con->$method();
        } else {
            header("HTTP/1.1 404 Not Found");
            header("status: 404 Not Found");
        }
    }

    /**
     * 程序运行执行的一些检测 - 框架自检
     */
    public function check()
    {
        /*

        define('ROOT_PATH', dirname(__DIR__) . '/')
        define('MODULE_NAME', 'admin')

        Conf::$all['view_path']
        Conf::$all['log_type']
        Conf::$all['log_path']
        Conf::$all['runtime_path']
        Conf::$all['page_length']

        */
    }
}
