<?php

/**
 * 配置文件操作类
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-01
 */

namespace fw\base;

class Conf
{
    public static $all = [];

    private static $_marks = [];

    /**
     * 加载配置文件
     *
     * @param array $files 要加载的文件
     */
    public static function load($files = ['common'])
    {
        foreach ($files as $f) {
            if (in_array($f, self::$_marks)) {
                continue;
            }

            $conf = require ROOT_PATH . 'conf/' . $f . '.php';

            self::$all = array_merge(self::$all, $conf);
            array_push(self::$_marks, $f);
        }
    }
}
