<?php

/**
 * 自动加载
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-05-31
 */

namespace fw\base;

class Autoload
{
    public static function register()
    {
        spl_autoload_register(__CLASS__ . '::load', true, false);
    }

    public static function load($className)
    {
        $relative_class = trim($className, '\\');

        $file = ROOT_PATH . str_replace('\\', DIRECTORY_SEPARATOR, $relative_class) . '.php';

        if (file_exists($file)) {
            require $file;
        }
    }
}
