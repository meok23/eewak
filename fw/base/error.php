<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>Error.</title>

    <style type="text/css">

        body {
            padding: 0 0 0 23px;
            font-family: 'Microsoft YaHei';
        }

        a {
            color: #27a1c5;
        }

        .emoji {
            float: left;
            padding: 0 23px 0 0;
        }

        .msg {
            float: left;
        }

        .jump > a {
            display: inline-block;
            padding-right: 14px;
        }
    </style>
</head>
<body>

<h1 class="emoji"> ←_← </h1>

<div class="msg">
    <p>
        <?php
        if (!empty($msg)) {
            echo $msg;
        } else {
            echo 'Error.';
        }
        ?>
    </p>

    <p class="jump">
        <a href="javascript:history.go(-1);">返回上一页</a>
        <a href="<?php echo Url::to(); ?>">返回首页</a>
    </p>
</div>

</body>
</html>
