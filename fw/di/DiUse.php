<?php

/**
 * 使用依赖注入
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-02
 */

namespace fw\di;

interface  DiUse
{
    public function setDi($di);
    public function getDi();
}
