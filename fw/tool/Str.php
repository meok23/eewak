<?php

/**
 * 字符串 工具类
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-05
 */

namespace fw\tool;

class Str
{
    /**
     * 过滤 markdown 标签，并截取指定的字数
     *
     * @param $subject
     * @param $len
     *
     * @return string
     */
    public static function stripMd($subject, $len)
    {
        $patterns = '/[\#\*\>_`|~]/';
        $subject = preg_replace($patterns, '', $subject);

        return mb_substr($subject, 0, $len);
    }

    /**
     * 加盐加密
     *
     * @param $original
     * @param $salt
     *
     * @return string
     */
    public static function encrypt($original, $salt)
    {
        return md5(sha1($original) . $salt);
    }

    /**
     * 下划线转驼峰
     *
     * @param string $str
     *
     * @return string
     */
    public static function lineToHump($str)
    {
        $str = preg_replace_callback('/([-_]+([a-z]{1}))/i', function ($matches) {
            return strtoupper($matches[2]);
        }, $str);
        return $str;
    }

    /**
     * 驼峰转下划线
     *
     * @param string $str
     *
     * @return string
     */
    public static function humpToLine($str)
    {
        $str = preg_replace_callback('/([A-Z]{1})/', function ($matches) {
            return '_' . strtolower($matches[0]);
        }, $str);

        return trim($str, '_');
    }
}
