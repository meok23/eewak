<?php

/**
 * Html 工具类
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-01
 */

namespace fw\tool;

class Html
{
    /**
     * xss 过滤器。此过滤器只适合出库时调用
     *
     * @param string $subject
     *
     * @return mixed
     */
    public static function xssFilter($subject)
    {
        $pattern = array(
            '#<iframe([^>]*)>(.*)</iframe([^>]*)>#is',
            '#<frame([^>]*)>(.*)</frame([^>]*)>#is',
            '#<script([^>]*)>(.*)</script([^>]*)>#is',
            '#<head([^>]*)>(.*)</head([^>]*)>#is',
            '#<title([^>]*)>(.*)</title([^>]*)>#is',
            '#<meta([^>]*)>#is',
            '#<link([^>]*)>#is',
        );

        $replacement = array(
            '&lt;iframe$1&gt;$2&lt;/iframe$3&gt;',
            '&lt;frame$1&gt;$2&lt;/frame$3&gt;',
            '&lt;script$1&gt;$2&lt;/script$3&gt;',
            '&lt;head$1&gt;$2&lt;/head$3&gt;',
            '&lt;title$1&gt;$2&lt;/title$3&gt;',
            '&lt;meta$1&gt;',
            '&lt;link$1&gt;',
        );

        return $subject = preg_replace($pattern, $replacement, $subject);
    }
}
