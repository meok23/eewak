<?php

/**
 * Json 工具类
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-01
 */

namespace fw\tool;

class Json
{
    /**
     * json_encode() 的二次封装
     *
     * @param mixed $data
     * @param null  $options
     *
     * @return string
     */
    public static function encode($data, $options = null)
    {
        if (!is_null($options)) {
            $ret = json_encode($data, $options);
        } elseif (version_compare(PHP_VERSION, '5.4.0', '>=')) {
            $ret = json_encode($data, JSON_UNESCAPED_SLASHES);
        } else {
            $ret = json_encode($data);
        }

        return $ret;
    }
}
