<?php

/**
 * 数组 工具类
 *
 * @author 煤老板 <meok23@sina.com>
 * @date   2017-06-07
 */

namespace fw\tool;

class Arr
{
    /**
     * 模拟 array_column 系统函数
     *
     * @param $rows
     * @param $column_key
     *
     * @return array
     */
    public static function column($rows, $column_key)
    {
        if (version_compare(PHP_VERSION, '5.5.0', '>=')) {
            $ret = array_column($rows, $column_key);
        } else {
            $ret = [];
            foreach ($rows as $row) {
                $ret[] = $row[$column_key];
            }
        }

        return $ret;
    }
}
