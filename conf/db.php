<?php

$db_conf = [

    // 数据库配置
    'db_conf' => [
        'host'    => '127.0.0.1',
        'port'    => '3306',
        'dbname'  => 'eewak',
        'user'    => 'root',
        'pwd'     => 'toor',
        'charset' => 'utf8',
    ],
];

// 连接数据库
\fw\db\DB::conn($db_conf['db_conf']);

return $db_conf;
