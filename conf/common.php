<?php

return [

    // URL相关配置
    'is_rewrite'       => false,       // 是否重写URL
    'front_script'     => 'index.php', // 前台的入口文件

    // 路径
    'view_path'        => ROOT_PATH . 'app/' . MODULE_NAME . '/view/',
    'runtime_path'     => ROOT_PATH . 'runtime/',

    // 邮件服务配置
    'mail'             => [
        'nickname' => '',    // 邮箱昵称，没设置就填 e-mail 地址
        'email'    => '',    // 发信人的 e-mail 地址
        'password' => '',    // 发信人的 e-mail 密码
        'host'     => '',    // 发信人的邮箱服务器
        'port'     => 25,
    ],

    // 评论回复是否邮件通知
    'is_comment_mail'  => false,
    'msg_mail_subject' => 'eewak 评论回复',

    // 网站标题
    'title'            => 'eewak.',
];
