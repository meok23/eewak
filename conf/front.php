<?php

return [

    // 设置 URL 重写模式
    'is_rewrite'       => true,

    // 默认首页
    'index_resource'   => 'Art/index',

    // seo 设置
    'seo_keyword'      => '',
    'seo_desc'         => '',

    // 文章列表长度
    'front_art_length' => 15,

    // 文章是否可评论
    'art_is_comment'   => true,

    // 评论列表长度
    'front_comment_length' => 15,
];
