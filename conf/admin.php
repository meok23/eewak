<?php

return [

    'page_length' => 20, // 分页列表长度
    'super_user'  => 101,  // 超级用户的id

    // 后台菜单
    'admin_menu'  => [
        [
            'title'  => '系统',
            '_child' => [
                [
                    'title' => '管理员',
                    'url'   => 'Manager/browse',
                ],
            ],
        ],
        [
            'title'  => '内容',
            '_child' => [
                [
                    'title' => '查看评论',
                    'url'   => 'Comment/browse',
                ],
                [
                    'title' => '文章',
                    'url'   => 'Art/browse',
                ],
                [
                    'title' => '独立页面',
                    'url'   => 'Page/browse',
                ],
                [
                    'title' => '文档集',
                    'url'   => 'Doc/browse',
                ],
            ],
        ],
    ],
];
