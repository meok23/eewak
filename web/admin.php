<?php

// 系统目录的定义
define('ROOT_PATH', dirname(__DIR__) . '/');
define('MODULE_NAME', 'admin');
define('WEB_PATH', __DIR__ . '/');

// 设置时区
date_default_timezone_set("PRC");

// 注册自动加载方法
require ROOT_PATH . 'fw/base/Autoload.php';
\fw\base\Autoload::register();

// 加载配置文件
\fw\base\Conf::load(['common', 'db', 'log', 'admin']);

// 实例化调用控制器
\fw\base\Rout::gi()->entry();
