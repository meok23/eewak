/**
 * 闪烁消息
 * @returns {number} id_of_setinterval
 */
function noticeGlint() {
    // 默认参数
    var sec = arguments[0] ? parseInt(arguments[0]) : 0;

    return self.setInterval(function () {
        switch (sec) {
            case 0:
                $('#form_notice').html('正在提交。');
                break;
            case 1:
                $('#form_notice').html('正在提交。。');
                break;
            case 3:
                $('#form_notice').html('正在提交。。。');
            default:
                break;
        }
        sec++;
        if (sec > 3) sec = 0;
    }, 300);
}

/**
 * 提交表单
 */
function ajaxSubmit() {

    // 接收参数
    var obj = arguments[0];

    // 校验
    if (!_validate($("#ajax_form"))) {
        return false;
    }

    // 禁用提交按钮
    $(obj).attr("disabled", "disabled").addClass("btn-disabled");

    // 闪烁消息提醒，一般在网络慢的时候才会体现出来
    var id_of_setinterval = noticeGlint();

    // 数据准备
    var data = $('#ajax_form').serialize();
    var url = $(obj).data('url');
    var jump = $(obj).data('jump');

    // 提交请求
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        success: function (res) {
            window.clearInterval(id_of_setinterval); // 停掉闪烁消息

            if (0 === res.code) {
                $('#form_notice').html('提交成功。');

                // 跳转
                switch (jump) {
                    case 'refresh':
                        history.go(0);
                        break;
                    case 'back':
                        history.go(-1);
                        break;
                    default:
                        break;
                }
            } else if (403 === res.code) {
                $('#form_notice').html('没有权限。');
            } else {
                $('#form_notice').html('操作失败。' + res.msg);
            }
        },
        error: function () {
            window.clearInterval(id_of_setinterval); // 停掉闪烁消息
            $('#form_notice').html('ajax 错误.');
        },
        complete: function () {
            // 重新开启提交按钮
            $(obj).removeAttr("disabled").removeClass("btn-disabled");
        }
    });
}

/**
 * 执行批量删除
 */
function ajaxDel() {

    // 接收参数
    var obj = arguments[0];

    // 校验
    if (!_validate($("#ajax_form"))) {
        return false;
    }

    // 禁用提交按钮
    $(obj).attr("disabled", "disabled").addClass("btn-disabled");

    // 闪烁消息提醒，一般在网络慢的时候才会体现出来
    var id_of_setinterval = noticeGlint();

    // 获取选中的条目
    var ids = [];
    $('input[data-checked]').each(function (index, element) {
        if (true === $(element).prop('checked')) {
            var manager_id = parseInt($(element).val());
            ids.push(manager_id);
        }
    });
    if (ids.length < 1) {
        alert('没有任何选择');
        window.clearInterval(id_of_setinterval); // 停掉闪烁消息
        return;
    }

    // 数据准备
    var url = $(obj).data('url');
    var manager_password = $('input[name="manager_password"]').val();
    var data = {"manager_password": manager_password, "ids": ids.toString()};

    // 提交请求
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        success: function (res) {
            window.clearInterval(id_of_setinterval); // 停掉闪烁消息

            if (0 === res.code) {
                $('#form_notice').html('提交成功。');

                // 返回上一页
                history.go(-1);
            } else if (403 === res.code) {
                $('#form_notice').html('没有权限。');
            } else {
                $('#form_notice').html('操作失败。' + res.msg);
            }
        },
        error: function () {
            window.clearInterval(id_of_setinterval); // 停掉闪烁消息
            $('#form_notice').html('ajax 错误.');
        },
        complete: function () {
            // 重新开启提交按钮
            $(obj).removeAttr("disabled").removeClass("btn-disabled");
        }
    });
}

/**
 * 跳转到批量删除
 */
function goDel() {
    var obj = arguments[0];

    var id = $(obj).data('id');
    var ids = [];

    if (id > 0) {
        ids.push(id);
    } else {
        $('input[data-checked]').each(function (index, element) {
            if (true === $(element).prop('checked')) {
                var id = parseInt($(element).val());
                ids.push(id);
            }
        });
    }

    if (ids.length > 0) {
        var url = $(obj).data('url');
        location.href = url + '&ids=' + ids.toString();
    } else {
        alert('没有任何选择');
    }
}
