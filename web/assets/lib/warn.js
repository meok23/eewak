/**
 * 弹出层警告
 *
 * @author 煤老板 <meok23@sina.com>
 * @date 2017-03-07
 */
$.extend({
    warn: function (options) {

        options = $.extend({
            msg: '',
            top: '20px',
            timeout: 1300
        }, options);

        // 创建警告层
        var warnBox = $('<div><button>&#xD7;</button><p>' + options.msg + '</p></div>');
        $('body').append(warnBox);

        // 警告层设置样式
        warnBox.css({
            "width": "480px",
            "position": "fixed",
            "left": "0px",
            "right": "0px",
            "top": options.top,
            "z-index": "9999999",
            "padding": "14px 0",
            "margin": "20px auto 0 auto",
            "border": "solid 2px #52B4D1",
            "border-radius": "4px",
            "text-shadow": "0px 1px 0px rgba(255, 255, 255, 0.5)",
            "text-align": "right",
            "color": "#ff0000",
            "background-color": "#DDECF1",
            "display": "block"
        });
        warnBox.children('button').css({
            "line-height": "20px",
            "position": "relative",
            "right": "15px",
            "top": "-10px",
            "background": "transparent none repeat scroll 0px 0px",
            "border": "0px none",
            "cursor": "pointer",
            "padding": "0px",
            "color": "#000000",
            "float": "right",
            "font-size": "20px",
            "font-weight": "bold",
            "opacity": "0.2",
            "text-shadow": "0px 1px 0px rgb(255, 255, 255)"
        });
        warnBox.children('p').css({
            "text-align": "center"
        });

        // 关闭警告层
        var closeWarn = function () {
            warnBox.remove();
        };

        warnBox.on('click', function () {
            closeWarn();
        });

        setTimeout(function () {
            closeWarn();
        }, options.timeout);
    }
});
