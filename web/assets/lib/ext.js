
// 对Date的扩展，将 Date 转化为指定格式
// 模仿 PHP 的 date('Y-m-d H:i:s');
// 例子：(new Date()).Format("Y-m-d H:i:s")
Date.prototype.Format = function (fmt) {
    var o = {
        "m": this.getMonth() + 1,  // 月
        "d": this.getDate(),       // 日
        "H": this.getHours(),      // 时
        "i": this.getMinutes(),    // 分
        "s": this.getSeconds(),    // 秒
    };

    // 设置年份
    if (/(Y)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, "" + this.getFullYear());
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }

    return fmt;
};

String.prototype.trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
};
String.prototype.ltrim = function () {
    return this.replace(/(^\s*)/g, "");
};
String.prototype.rtrim = function () {
    return this.replace(/(\s*$)/g, "");
};
