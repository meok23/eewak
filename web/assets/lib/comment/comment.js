/**
 * 评论
 *
 * @author 煤老板 <meok23@sina.com>
 * @date 2017-04-02
 */
var comment = {

    conf: {
        api: "",
        expire: 30,
        word: ["管理员", "admin", "root"]
    },

    build: function (conf) {
        comment.conf = $.extend(comment.conf, conf);

        comment.loadComment();
    },

    /* 加载评论 */
    loadComment: function () {
        var comment_url = comment.conf.api;
        var page = 1;
        if (arguments.length > 0) {
            page = arguments[0];
        }

        $.get(comment_url, {page: page}, function (res) {
            if (res.code === 0) {

                var htm = '';
                var d = res.data;
                $.each(d.data, function (index, ele) {
                    var tpl = comment.tplComment(ele);
                    htm += tpl;
                });

                var htm_page = comment.tplPage(d.current_page, d.page_total, d.list_count);
                htm += htm_page;
                $('#comment_show').html(htm);
            }
        }, 'json');
    },

    /* 分页的模板 */
    tplPage: function (current_page, page_total, list_count) {
        if (list_count < 1) {
            return '';
        }

        var last = '';
        if (current_page <= 1) {
            last = '<a href="javascript:;">&lt;</a>';
        } else {
            var page = parseInt(current_page) - 1;
            last = '<a href="javascript:;" onclick="comment.loadComment(' + page + ');">&lt;</a>';
        }

        var next = '';
        if (current_page >= page_total) {
            next = '<a href="javascript:;">&gt;</a>';
        } else {
            var page = parseInt(current_page) + 1;
            next = '<a href="javascript:;" onclick="comment.loadComment(' + page + ');">&gt;</a>';
        }

        var tpl = '<div class="comment-page">'
            + '<ul>'
            + '<li><span>共' + list_count + '条 ' + current_page + '/' + page_total + '页</span></li>'
            + '<li>' + last + '</li>'
            + '<li>' + next + '</li>'
            + '</ul>'
            + '</div>';

        return tpl;
    },

    /* 评论列表模板 */
    tplComment: function (data) {
        var tpl = '<div class="item">'
            + '<div class="top">'
            + '<b>' + data.nickname + '</b>'
            + '<time>' + new Date(data.create_time * 1000).Format('Y-m-d H:i') + '</time>'
            + '<a href="javascript:;" data-id="' + data.comment_id + '" onclick="comment.commentQuote(this);"># 引用</a>'
            + '</div>';
        var tpl_quote = '';
        if (data.quote.length > 1) {
            tpl_quote += '<blockquote>'
                + '<pre>引用 ' + data.quote.replace(/^`(.*)`.*/, "$1") + ' 的发言</pre>'
                + data.quote.replace(/^`(.*)`(.*)/, "$2")
                + '</blockquote>';
        }
        tpl = tpl + tpl_quote
            + '<div class="content">'
            + data.content
            + '</div>'
            + '</div>';

        return tpl;
    },

    /* 引用评论 */
    commentQuote: function () {
        var that = arguments[0];
        var item = $(that).closest('.item');

        var comment_id = $(that).data('id');
        var nickname = item.find('.top>b').text();
        var txt = item.children('.content').text();

        var htm = '<blockquote>'
            + '<pre>引用 ' + nickname + ' 的发言</pre>'
            + txt
            + '</blockquote><a class="close" href="javascript:;" onclick="comment.closeQuote();">&#xD7;</a>';

        $('#to_comment_id').val(comment_id);
        $('#quote_preview').html(htm);
    },

    /* 关闭引用 */
    closeQuote: function () {
        $('#to_comment_id').val(0);
        $('#quote_preview').empty();
    },

    /* 提交评论 */
    commentSubmit: function () {

        // 接收参数
        var that = arguments[0];

        // 防灌水
        if (comment.avertWater() === false) {
            alert('评论太频繁，请稍后操作。');
            return false;
        }

        // 称呼过滤
        var nickname = $("#comment_form").find('input[name=nickname]').val();
        if (comment.filterWord(nickname) === false) {
            var word = (comment.conf.word).join('/');
            alert('称呼不可包含：' + word + ' 等字眼。');
            return false;
        }

        // 校验
        if (!_validate($("#comment_form"))) {
            return false;
        }

        // 数据准备
        var data = $('#comment_form').serialize();
        var url = $(that).data('url');

        // 提交请求
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            success: function (res) {
                if (0 === res.code) {
                    $('#comment_form_notice').html('评论成功。');

                    // 标识灌水
                    comment.markWater();

                    // 清除前一次引用
                    comment.closeQuote();

                    // 更新列表
                    var info = comment.tplComment(res.data);
                    $("#comment_show").prepend(info);

                } else if (403 === res.code) {
                    $('#comment_form_notice').html('没有权限。');
                } else {
                    $('#comment_form_notice').html('操作失败。' + res.comment);
                }
            },
            error: function () {
                $('#comment_form_notice').html('ajax 错误.');
            }
        });
    },

    /* 防灌水 */
    avertWater: function () {
        var c_name = 'comment_art_avert_water';
        var c_val = getCookie(c_name);

        if (c_val !== null) {
            return false;
        }

        return true;
    },

    /* 标识灌水 */
    markWater: function () {
        var c_name = 'comment_art_avert_water';
        setCookie(c_name, 'water', comment.conf.expire);
    },

    /* 关键词过滤 */
    filterWord: function (txt) {
        var data = comment.conf.word;

        txt = txt.toLowerCase().trim();

        for (var k in data) {
            if (new RegExp(data[k]).test(txt)) {
                return false;
            }
        }

        return true;
    }
};
