/**
 * 拖动排序，而且可以修改上级关系
 *
 * @author 煤老板 <meok23@sina.com>
 * @date 2017-02-23
 */
(function ($) {
    var dropEle = $();
    $.fn.sortable = function (options) {

        options = $.extend({
            success: function (res) {
                console.log(res);
            }
        }, options);

        var items = $(this).find('li');

        items.attr('draggable', 'true').on('selectstart', function () {

            // IE浏览器不支持 draggable 属性，但可通过事件处理程序调用 dragDrop() 方法来实现拖动效果
            // 注意：需要使用 selectstart事件，mousedown事件，高版本IE会闪屏
            this.dragDrop && this.dragDrop();

            return false;
        }).on('dragstart', function (e) {
            e = e || window.event;

            var dt = e.originalEvent.dataTransfer;

            // 设置允许的拖拽效果
            dt.effectAllowed = 'all';

            // 让firefox支持draggable属性，必须使用dragstart事件处理程序，并在dataTransfer对象使用setData()方法来启动效果
            // setData 第二个参数填空，禁用拖拽搜索
            dt.setData('Text', '');

            // dropEle 拖放源的引用
            (dropEle = $(this)).addClass('sortable-dropEle');

            // 阻止冒泡
            e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);
        }).add(this).on('dragover drop', function (e) {
            e = e || window.event;

            // 判断拖放源是否在items集合内
            if (!items.is(dropEle)) {
                return false;
            }

            e.preventDefault();
            e.originalEvent.dataTransfer.dropEffect = 'move';

            var target = $(this).filter('li');
            target.addClass('sortable-target');

            // 阻止冒泡
            e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);

            // 元素被放到了拖放目标中
            if (e.type == 'drop' && $(this).is(dropEle.siblings())) {
                target.after(dropEle);

                dropEle.removeClass('sortable-dropEle');
                target.removeClass('sortable-target');

                options.success(dropEle);
                dropEle = null;

                // return false 有preventDefault()的作用
                return false;
            }

            if(!$(this).is(dropEle.siblings())) {
                target.removeClass('sortable-target');
            }

            return false;
        }).on('dragleave dragend', function() {

            $(this).removeClass('sortable-target');
        });
    };
})(jQuery);
