/**
 * 设置 cookie
 * @param c_name  键
 * @param value   值
 * @param expire  过期时长，与php的setcookie不同
 */
function setCookie(c_name, value, expire) {
    var exp = new Date();
    exp.setTime(exp.getTime() + expire * 1000);

    document.cookie = c_name + "=" + encodeURI(value) + ((expire == null) ? "" : ";expires=" + exp.toGMTString() + "");
}

function getCookie(c_name) {
    var arr, reg = new RegExp("(^| )" + c_name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
        return decodeURI(arr[2]);
    } else {
        return null;
    }
}

function delCookie(c_name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var c_val = getCookie(c_name);
    if (c_val != null) {
        document.cookie = c_name + "=" + c_val + ";expires=" + exp.toGMTString();
    }
}
