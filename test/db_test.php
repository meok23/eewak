<?php

define('ROOT_PATH', dirname(__DIR__) . '/');

require ROOT_PATH . 'fw/base/Autoload.php';
\fw\base\Autoload::register();

use \fw\db\DB;

$start_time = microtime(true);

$conf = array(
    'host'     => '127.0.0.1',
    'port'     => '3306',
    'dbname'   => 'test',
    'user'     => 'root',
    'pwd'      => 'root',
    'charset'  => 'utf8',
    'is_debug' => false,
);
DB::conn($conf);

$rt = DB::queryRow("SELECT * FROM test.txttest WHERE id=2 AND valtxt='中'");
print_r($rt);

$consume_time = microtime(true) - $start_time;

header("Content-type:text/html;charset=utf-8");
echo '<br>' . 'consume_time' . $consume_time;
