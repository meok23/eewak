-- MySQL dump 10.14  Distrib 5.5.52-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: eewak
-- ------------------------------------------------------
-- Server version	5.5.52-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `art`
--

DROP TABLE IF EXISTS `art`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art` (
  `art_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `manager_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `tags` varchar(255) NOT NULL DEFAULT '',
  `is_hide` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  PRIMARY KEY (`art_id`),
  KEY `is_hide` (`is_hide`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COMMENT='文章表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art`
--

LOCK TABLES `art` WRITE;
/*!40000 ALTER TABLE `art` DISABLE KEYS */;
INSERT INTO `art` VALUES (118,'Linux安装笔记','官网下载的镜像 CentOS-7-x86_64-DVD-1611.iso\r\n\r\n## 新建虚拟机\r\n\r\n新建虚拟机时记得选 CentOS 64 位，不然驱动无法正确安装，则网卡异常\r\n\r\n## 安装信息摘要\r\n\r\n日期和时间：亚洲上海\r\n\r\n软件选择：基本网页服务\r\n[√]perl\r\n[√]基本开发工具\r\n[√]Java\r\n\r\n网络和主机名：ON\r\n\r\n## 安装 VMware Tools\r\n\r\n点击VMware的菜单，重新安装 VMware Tools；\r\nLinux操作：挂载光驱、复制文件、解压、进入解压后的目录执行./vmware-install.pl。\r\n然后，reboot，结果还是访问不了共享目录。具体操作可以参考下面这篇文章：\r\n\r\nhttps://www.ytyzx.org/index.php/如何在Linux(CentOS_7)命令行模式安装VMware_Tools\r\n\r\n---\r\n\r\n若干折腾之后，发现，centos7 根本就不需要安装 `VMware Tools`，而是使用 `open-vm-tools`，默认装好的，参看如下文章：\r\n\r\nhttp://www.linuxdiyf.com/linux/25102.html\r\n\r\n[挂载 共享目录]\r\n```\r\nvmhgfs-fuse -o allow_other .host:/$(vmware-hgfsclient) /mnt/hgfs\r\n\r\n或者\r\n\r\nmount -t fuse.vmhgfs-fuse -o allow_other .host:/$(vmware-hgfsclient) /mnt/hgfs\r\n```\r\n\r\n### 小结\r\ncentos7- 版本，像以往那样安装 `VMware Tools` 就行了，上面有教程链接；\r\ncentos7+ 版本，不需要额外安装 `VMware Tools`；\r\n\r\n> 有人说，centos7+ 也可以安装 `VMware Tools`，只是我的 VMware 版本太低（11）\r\n\r\n# 系统设置\r\n\r\n- `/etc/rc.d/rc.local`[注意执行权限]\r\n\r\n---\r\n---\r\n\r\n# 软件安装\r\n\r\n## 安装 Nginx\r\n\r\n### 准备 yum 仓库\r\n\r\nTo add NGINX yum repository, create a file named /etc/yum.repos.d/nginx.repo and paste one of the configurations below:\r\n\r\n```\r\n[nginx]\r\nname=nginx repo\r\nbaseurl=http://nginx.org/packages/centos/$releasever/$basearch/\r\ngpgcheck=0\r\nenabled=1\r\n```\r\n\r\n上面准备好了 Nginx 仓库，可以使用 yum 安装了，不过，我想使用源码安装\r\n\r\n安装Nginx依赖包：`yum-builddep nginx`\r\n\r\n### 开始安装\r\n\r\n把源码包复制到 /usr/local/src 并解压，进入解压后的目录\r\n\r\n```\r\n./configure\r\nmake\r\nmake install 2>&1 | tee install-20170519.log\r\nmake clean\r\n```\r\nBy default, NGINX will be installed in /usr/local/nginx.\r\n\r\n### 配置\r\n\r\n配置其实蛮简单的, `/usr/local/nginx/conf/nginx.conf`\r\n\r\n执行 `/usr/local/nginx/sbin/nginx`，并将其写入 `/etc/rc.d/rc.local`[注意执行权限]\r\n关闭使用 `/usr/local/nginx/sbin/nginx -s stop` 命令\r\n\r\n外网访问超时，在终端执行 curl localhost 能正常访问，猜测是防火墙的问题\r\n```\r\nfirewall-cmd --add-port=80/tcp --permanent\r\nfirewall-cmd --reload\r\n```\r\n\r\n## 安装 mariadb\r\n\r\n```\r\nyum remove mariadb*\r\nyum -y install mariadb-server\r\n\r\nsystemctl enable mariadb.service\r\nsystemctl start mariadb.service\r\n```\r\n\r\n如果只执行`systemctl start`不执行`systemctl enable`，会报错`Can\'t connect to local MySQL server through socket \'/var/lib/mysql/mysql.sock\'`\r\n\r\n执行`mysql_secure_installation`，做一些初始化配置：\r\na)为root用户设置密码\r\nb)删除匿名账号\r\nc)取消root用户远程登录\r\nd)删除test库和对test库的访问权限\r\ne)刷新授权表使修改生效\r\n\r\n\r\n## 安装 PHP\r\n\r\n安装php依赖包：`yum-builddep php`\r\n\r\nyum -y install readline-devel\r\n\r\n### 开始安装\r\n\r\n把源码包复制到 /usr/local/src 并解压，进入解压后的目录\r\n\r\n```\r\n./configure --prefix=/usr/local/php --with-config-file-path=/usr/local/php/etc --with-config-file-scan-dir=/usr/local/php/etc/php.d --with-pdo-mysql=mysqlnd --with-gd --with-jpeg-dir --with-curl --with-readline --with-openssl --with-zlib --with-libzip --enable-mbstring --enable-sockets --enable-bcmath --enable-fpm\r\n\r\n# 需要清楚上面的每一项的意思，./configure --help | grep xxx\r\n\r\n# --prefix=/usr/local/php php安装路径\r\n# --with-config-file-path=/usr/local/php/etc php配置文件路径\r\n# --with-config-file-scan-dir=/usr/local/php/etc/php.d php配置文件子路径，分布各个模块的配置文件\r\n# --with-readline php命令交互支持\r\n# --with-openssl 443支持，比如pecl安装\r\n# --with-jpeg-dir gd库支持JPG\r\n\r\nmake\r\nmake test\r\nmake install 2>&1 | tee install-20170621.log\r\nmake clean\r\n```\r\n\r\n### 配置\r\n\r\n```\r\ncp /usr/local/src/php/php-7.1.5/php.ini-development /usr/local/php/etc/php.ini\r\ncp /usr/local/php/etc/php-fpm.conf.default /usr/local/php/etc/php-fpm.conf\r\ncp /usr/local/php/etc/php-fpm.d/www.conf.default /usr/local/php/etc/php-fpm.d/www.conf\r\n\r\nmake /usr/local/php/etc/php.d\r\ntouch /usr/local/php/etc/php.d/ext.ini\r\n\r\ngroupadd www\r\nuseradd -g www www\r\n```\r\n\r\n修改 www.conf\r\n```\r\nuser = www\r\ngroup = www\r\n```\r\n\r\n执行 `/usr/local/php/sbin/php-fpm`，并将其写入 `/etc/rc.d/rc.local`[注意执行权限]\r\n\r\n验证\r\n```\r\nphp -m\r\nphp --ini\r\nps -ef | grep php-fpm\r\nnetstat -tnl | grep 9000\r\n```\r\n\r\n## php 扩展安装\r\n\r\n### 使用 composer 的时候，发现需要 zlib\r\n\r\n```\r\ncd /usr/local/src/php/php-7.1.5/ext/zlib/\r\ncp config0.m4 config.m4\r\nphpize\r\n./configure --with-php-config=/usr/local/php/bin/php-config\r\n\r\n报warning: You will need re2c 0.13.4 or later if you want to regenerate PHP parsers\r\n\r\nyum install -y re2c\r\n./configure --with-php-config=/usr/local/php/bin/php-config\r\nmake\r\nmake install\r\n\r\necho \'extension=zlib.so\' >> /usr/local/php/etc/php.d/ext.ini\r\n```\r\n\r\n### 项目需要 bcmath\r\n\r\n```\r\ncd /usr/local/src/php/php-7.1.5/ext/bcmath\r\nphpize\r\n./configure --with-php-config=/usr/local/php/bin/php-config\r\nmake\r\nmake install\r\n```\r\n\r\n## 安装 memcache\r\n\r\n服务\r\n```\r\nyum -y install memcached\r\nsystemctl enable memcached.service\r\nsystemctl start memcached.service\r\n```\r\n\r\n管理\r\n```\r\nyum -y install telnet\r\ntelnet 127.0.0.1 11211\r\nquit\r\n```\r\n\r\n安装php的memcached扩展\r\n```\r\n解压 libmemcached 的源码，并进入解压后的目录\r\n./configure --prefix=/usr/local/libmemcached --with-memcached\r\nmake\r\nmake install 2>&1 | tee install-20170621.log\r\nmake clean\r\n\r\npecl install memcached\r\n在 libmemcached directory [no] 处输入 /usr/local/libmemcached\r\n```',101,1498665600,1510645897,'linux',0),(121,'测试','黑盒测试，看不到代码，测试人员QA负责；\r\n白盒测试，看得到代码，程序员自己负责。\r\n\r\nQA的测试一般是全局测试，程序员自己的测试一般是单元测试。\r\n\r\n什么是单元测试，一个不规范的说法就是：每个函数的独立测试。当然，实际上不止函数，还有sql、正则、switch、独立脚本...\r\n\r\n什么时候进行单元测试：一个小单元开发完成之后，代码提交之前。\r\n\r\n为什么要进行单元测试：更容易发现问题、找到问题的代价更低、更容易想到解决方案...\r\n\r\n### 1）懵懂入门，边写边测\r\n在刚刚开始学编程的时候，不了解什么是单元测试。因为那时候，每敲一行代码都要刷新或者编译执行一下，这不只是单元测试了，甚至都是原子测试了，当然，原子测试是我瞎掰的。所以那个时候是不自觉的进行了单元测试的，不过这种单元测试是没有规划的，不全面的，有些情况还是覆盖不到的。\r\n\r\n### 2）登堂入室，写好再测\r\n每敲一行代码就编译执行一遍，效率低的抓狂。后面慢慢成长起来了，可以一口气写几个模块了。一口气写下来，中间是没有像以前那样一行行测试的了。集成到系统里面去，全局去执行，像普通用户一样去操作，然后会发现很多问题，特别多问题，然后一个个改，改完之后再去执行，出错再改，循环反复，改的时间比最初的开发时间还长。发现没有？这里面没有单元测试。\r\n\r\n### 3）蓦然回首，重拾单元测试\r\n一口气写完几个模块，写完之后，不要着急把代码集成到系统中。首先，规划好测试用例，每个方法、每个分支、每种情况都测一遍，尽量把每个角落都覆盖到位，这就是单元测试。测完之后，再浏览一遍代码，可能会小小修改一下的，修完之后，再执行一遍单元测试，这一遍应该是自动的。完了之后，才集成到系统里面去，这时候，全局去执行，一般都不会有什么问题的了。\r\n\r\n### 4）review代码更重要\r\n在写测试用例进行测试之前，先浏览一遍代码，通过人工过滤的方式找出一些问题。至少，注释的问题，程序是测不出来的，只能人工去发现。\r\n\r\n---\r\n\r\n[参考文档]\r\n- http://tech.sina.com.cn/s/2009-07-20/1529991506.shtml\r\n- http://blog.csdn.net/happylee6688/article/details/37962283\r\n- http://www.51testing.com/html/48/n-2471648.html\r\n\r\n## 番外篇\r\n\r\n我以前在流水线上干过无绳电话组装。每个电话由面板、底壳、主板、按键、喇叭、电池... 组成。\r\n在组装之前，如果没有对这些组件分别测试过，那极有可能装好之后的电话是不能用的。那么，拆掉，重新返工。\r\n\r\n对应编程，这里有两个概念：\r\n1. 组装之前每个组件的测试就是单元测试。\r\n2. 组装之后，测试电话能不能用就是全局测试。\r\n\r\n如果不认真做单元测试，组装好之后的全局测试发现的问题，是要拆机返工的。',101,0,0,'测试',0),(122,'理解什么是脚本','要理解什么是脚本，首先要理解什么是脚手架，\r\n脚手架一词最早出现在装修行业，就是梯子啦，工作台啦，锯木头的工作台，……\r\n\r\n所以脚手架就是一些工具，提高人的工作效率的工具，想象一下，没有梯子，装修会多辛苦哇，\r\n而脚本，就是代码的脚手架，就是用来提高程序员工作效率的代码片段，\r\n\r\n另外，由于解析型编程语言修改方便，编写脚本是最合适的，所以很多人误解以为脚本就是解析型语言写的代码。。\r\n嗯，以上，就是脚本的意思。',101,0,0,'',0);
/*!40000 ALTER TABLE `art` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapter`
--

DROP TABLE IF EXISTS `chapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapter` (
  `chapter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `doc_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联文档表',
  `content` text NOT NULL,
  `manager_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `types` char(10) NOT NULL DEFAULT 'content' COMMENT 'content:内容,dir:目录',
  PRIMARY KEY (`chapter_id`),
  KEY `doc_id` (`doc_id`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COMMENT='书的章节表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapter`
--

LOCK TABLES `chapter` WRITE;
/*!40000 ALTER TABLE `chapter` DISABLE KEYS */;
INSERT INTO `chapter` VALUES (123,'第一页',0,109,'第一页的内容。。',101,1503642420,0,0,'content'),(124,'第二页',0,109,'第二页的内容。。。',101,1503642569,0,0,'content'),(128,'第三页',0,109,'第三页的内容。。。。。',101,1504493695,0,0,'content'),(129,'第四页',0,109,'第四页的内容。。。。。',101,1504493750,0,0,'content'),(130,'二级目录1',0,109,'',101,1504493762,0,0,'dir'),(131,'二级目录2',0,109,'',101,1504493773,0,0,'dir'),(132,'第21页',130,109,'。。',101,1504493792,0,0,'content'),(133,'第22页',130,109,'。。。',101,1504493837,0,0,'content');
/*!40000 ALTER TABLE `chapter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel` char(20) NOT NULL DEFAULT '',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `obj_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联对象的id，比如：文章id',
  `to_comment_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论了哪条评论',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:未审核,1:审核通过,...',
  `quote` text NOT NULL,
  `content` text NOT NULL,
  `nickname` char(20) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(100) NOT NULL DEFAULT '' COMMENT '个人主页',
  `manager_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_admin` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:常规评论,1:管理员在后台回复的',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `obj_id` (`obj_id`),
  KEY `channel` (`channel`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COMMENT='评论表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc`
--

DROP TABLE IF EXISTS `doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc` (
  `doc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `brief` varchar(255) NOT NULL DEFAULT '',
  `manager_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_hide` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `types` char(10) NOT NULL DEFAULT 'doc' COMMENT 'doc:文档,url:外链',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '外链链接',
  PRIMARY KEY (`doc_id`),
  KEY `types` (`types`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='文档表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc`
--

LOCK TABLES `doc` WRITE;
/*!40000 ALTER TABLE `doc` DISABLE KEYS */;
INSERT INTO `doc` VALUES (101,'php for redis','',101,0,1492580263,0,'url','http://www.kancloud.cn/meok/p4r/236461'),(109,'rabbitmq笔记','',101,0,1503642380,0,'doc','');
/*!40000 ALTER TABLE `doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_error`
--

DROP TABLE IF EXISTS `log_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_error` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `breakpoint` varchar(50) NOT NULL DEFAULT '' COMMENT '断点，比如：函数名',
  `response` text NOT NULL COMMENT '序列化的响应值，一般是code+msg+data',
  `response_code` int(10) NOT NULL DEFAULT '0' COMMENT '响应码',
  `param` text NOT NULL COMMENT '函数参数',
  `check_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:未读',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COMMENT='错误日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_error`
--

LOCK TABLES `log_error` WRITE;
/*!40000 ALTER TABLE `log_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `manager_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `last_ip` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录IP',
  `last_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级Id',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:冻结,1:启用',
  `identities` char(32) NOT NULL DEFAULT '',
  `token` char(32) NOT NULL DEFAULT '',
  `timeout` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`manager_id`),
  UNIQUE KEY `signs` (`identities`)
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='管理员表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` VALUES (101,'煤老板','admin@c.com','4cd8e4730bf5f1114a343cf7ca6eecb3',3232249857,1510640743,1503640330,1471923514,0,1,'996f4f83b822b18e9e9195347be44144','4ccdc59976cbec317c1db7ca76578a1c',1510675200);
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` char(20) NOT NULL DEFAULT '' COMMENT '单页的标识',
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `manager_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `tags` varchar(255) NOT NULL DEFAULT '',
  `brief` text NOT NULL,
  `is_hide` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `is_hide` (`is_hide`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COMMENT='单页表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (101,'about','关于','## 关于网址\r\n\r\n记得很久之前，大概是14年3月份，那时候接触了几个概念：响应式、webApp等。我就想开发一款webApp框架出来，取名：web的w、App的A、框架的k，组成wak，中文名挖矿。最后，webApp框架没有做成，wak的名字却沿用了下来。\r\n\r\n至于为什么网址是 eewak.com？因为wak.com早就被注册了呀，所以，我就随便加了个前缀。\r\n\r\n## 关于源码\r\n\r\n使用原生php自建框架开发，源码托管在开源中国和GitHub\r\n\r\n- https://git.oschina.net/meok23/eewak.git\r\n\r\n- https://github.com/meok23/eewak.git\r\n\r\n使用`Apache License 2.0`协议，喜欢的拿去',101,1492490875,1503632797,'','   关于网址\r\n\r\n记得很久之前，大概是14年3月份，那时候接触了几个概念：响应式、webApp等。我就想开发一款webApp框架出来，取名：web的w、App的A、框架的k，组成wak，中文名挖矿。最后，webApp框架没有做成，wak的名字却沿用了下来。\r\n\r\n至于为什么网址是 eewak.com？因为wak.com早就被注册了呀，所以，我就随便加了个前缀。\r\n\r\n   关于源码\r\n\r\n使用',0);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recy`
--

DROP TABLE IF EXISTS `recy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recy` (
  `recy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `remark` varchar(255) NOT NULL DEFAULT '',
  `del_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '加入回收站的时间',
  `op_user` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作者',
  PRIMARY KEY (`recy_id`),
  KEY `del_time` (`del_time`)
) ENGINE=MyISAM AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COMMENT='回收站表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recy`
--

LOCK TABLES `recy` WRITE;
/*!40000 ALTER TABLE `recy` DISABLE KEYS */;
/*!40000 ALTER TABLE `recy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_rel`
--

DROP TABLE IF EXISTS `tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_rel` (
  `tag` char(50) NOT NULL DEFAULT '',
  `obj_id` int(10) unsigned NOT NULL,
  `channel` char(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`channel`,`obj_id`,`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='标签关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_rel`
--

LOCK TABLES `tag_rel` WRITE;
/*!40000 ALTER TABLE `tag_rel` DISABLE KEYS */;
INSERT INTO `tag_rel` VALUES ('linux',118,'art'),('测试',121,'art');
/*!40000 ALTER TABLE `tag_rel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-14 15:58:45
