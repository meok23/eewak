# 说明文档

## 简介

这原本是`eewak.com`的源码，自建的mvc框架，我把它叫做`挖矿框架`

## 目录树

```
.
├── app                     # 应用目录
│   ├── admin               # 后台应用
│   │   ├── controller      # 后台控制器
│   │   ├── model           # 后台模型
│   │   └── view            # 后台视图
│   ├── common              # 应用的公共部分
│   └── front               # 前台用于
├── conf                    # 配置目录
├── ext                     # 扩展目录，用来放一些php类库
├── fw                      # firework 框架目录
├── runtime                 # 运行时目录，可删
├── script                  # 一些备用文档与脚本
├── test                    # 测试目录
└── web                     # 网站入口目录
```

## 数据库

数据库文件：`script/data-simple.sql`

相关配置：`conf/db.php`

## 网站部署

主要是一些URL重写规则需要注意

- Apache部署：`web/.htaccess`文件直接使用
- Nginx部署：参考`script/nginx.conf`

## 系统要求

php5.6+，建议使用php7

## 快速启动

- 把源码上传到网站根目录
- `runtime`与`web/upload`目录赋予777权限
- 新建一个数据库，名字随意
- 导入`script/data-simple.sql`到数据库
- 修改`conf/db.php`数据库连接配置
- 访问`http://域名/web/admin.php`
- 账号：admin@c.com 密码：c.com

# 协议

Apache License 2.0
